-include $(PROJECT_PATH)/build/include/config/auto.conf

FORCE_LINK:=$(foreach mod,$(MODULE_NAMES),$(if $(CONFIG_LUA_MODULE_$(mod)), -u $(mod)_module_selected1))


COMPONENT_SRCDIRS:=auxlib lib luabind
COMPONENT_ADD_INCLUDEDIRS:=auxlib lib luabind
ifneq ($(CONFIG_LUA_MODULE_SDM),)
COMPONENT_ADD_LDFLAGS=-u SDM_module_selected1 -lsdm
endif

# build for old sdk
# CFLAGS += -DSDMSDK2
ifneq ($(CONFIG_SDM_DEVSPLTCHAR),)
CFLAGS += -DDEVSPLTCHAR=$(patsubst "%",\"%\",$(CONFIG_SDM_DEVSPLTCHAR))
endif
ifneq ($(CONFIG_SDM_PINNUM),)
CFLAGS += -DPINNUM=$(CONFIG_SDM_PINNUM)
endif

