local sdm = require "sdm"

--[[ Local attributes
   sdm.root
   sdm.device_handle
   sdm.local_attr_add
   sdm.local_attr_handle
   sdm.local_attr_remove
--]]

do
   assert(sdm.device_handle("NODEMCU") == nil)
   assert(sdm.root() == nil)
   sdm.init()
   local root = sdm.device_handle("NODEMCU")
   assert(root ~= nil)
   assert(root == sdm.root())
   assert(sdm.handle_type(root) == "device")
   assert(sdm.local_attr_add(root, "id", "id", "123456", test_self, nil) ~= nil)
   assert(sdm.local_attr_add(root, "id", "id", "123456", test_self, nil) == nil)
   assert(sdm.local_attr_handle(root, "id") ~= nil)
   local g, s = sdm.attr_func(sdm.local_attr_handle(root, "id"))
   assert(g == nil)
   assert(s == nil)
   assert(sdm.local_attr_remove(root, sdm.local_attr_handle(root, "id")))
   assert(sdm.local_attr_handle(root, "id") == nil)
   assert(not sdm.local_attr_remove(root, sdm.local_attr_handle(root, "id")))
   assert(sdm.local_attr_add(root, "id", "id", "123456", test_self, nil) ~= nil)
   sdm.destroy()
   assert(sdm.device_handle("NODEMCU") == nil)
   assert(sdm.root() == nil)
end

--[[ Driver poll
   sdm.device_add
   sdm.device_name
   sdm.device_basename
   sdm.device_poll
   sdm.device_parent
   sdm.request_name
   sdm.driver_attached
   sdm.devices
   sdm.attr_handle
   sdm.attr_copy
   sdm.driver_release
   sdm.device_prvt_attrs
   sdm.device_rename
   sdm.method_dev_handle
   sdm.prvt_attr_handle
   sdm.prvt_attr_remove
   sdm.attr_dev_handle
   sdm.device_methods
   sdm.device_local_attrs
--]]

do
   local function NODEMCU_poll(dev)
      return sdm.device_name(dev) == "NODEMCU"
   end

   local function NODEMCU_init(dev)
      assert(sdm.device_name(dev) == "NODEMCU")
   end

   local function onewire_poll(dev, driver, parent)
      local name, instance = sdm.device_basename(dev)
      assert(sdm.device_name(parent) == "NODEMCU")
      return name == "onewire"
   end

   local function onewire_init(dev)
   end

   sdm.init()
   local drv = sdm.driver_add("NODEMCU")
   sdm.method_add(drv, "_poll", nil, NODEMCU_poll)
   sdm.method_add(drv, "_init", "init", NODEMCU_init)
   assert(sdm.device_parent(sdm.root()) == nil)

   assert(sdm.device_poll(sdm.root()))
   assert(not sdm.device_poll(sdm.root()))

   local handle = sdm.device_add(sdm.request_name("onewire"), sdm.root())
   assert(handle ~= nil)
   assert(not sdm.device_poll(handle))
   assert(sdm.device_name(handle) == "onewire-0")
   assert(sdm.device_basename(handle) == "onewire")
   assert(sdm.driver_attached(handle) == nil)

   drv = sdm.driver_add("onewire")
   sdm.method_add(drv, "_poll", "poll", onewire_poll)
   sdm.method_add(drv, "_init", nil, onewire_init)
   local attr = sdm.attr_add(drv,
			     "attr1",
			     "attribute 1",
			     "string",
			     test_self,
			     nil)
   sdm.attr_add(drv,
		"attr2",
		"attribute 2",
		true,
		function() return "get" end,
		function() return "set" end)
   assert(attr ~= nil)

   assert(sdm.device_poll(handle))
   assert(sdm.driver_attached(handle) == drv)

   for i=1,10 do
      local handle = sdm.device_add(sdm.request_name("onewire"), sdm.root())
      assert(handle ~= nil)
      assert(sdm.device_poll(handle))
   end
   local devices = sdm.devices()
   assert(devices ~= nil)
   assert(devices["onewire-0"] ~= nil)
   assert(devices["onewire-1"] ~= nil)
   assert(devices["onewire-2"] ~= nil)
   assert(devices["onewire-100"] == nil)

   assert(sdm.device_handle("onewire-5") ~= nil)
   assert(not sdm.device_remove(sdm.device_handle("onewire-5")))
   assert(sdm.driver_release(sdm.device_handle("onewire-5")))
   assert(sdm.device_remove(sdm.device_handle("onewire-5")))
   assert(sdm.device_handle("onewire-5") == nil)

   assert(not sdm.driver_remove(sdm.driver_handle("onewire")))

   assert(sdm.device_parent(handle) == sdm.root())
   local phony = sdm.device_add("onewire-1", sdm.root())
   assert(phony == nil)
   handle = sdm.device_add("onewire-2", sdm.root())
   assert(handle == nil)
   assert(not sdm.device_poll(handle))
   handle = sdm.device_add(sdm.request_name("onewire"), sdm.root())
   assert(handle ~= nil)
   assert(sdm.device_poll(handle))
   local attr = sdm.attr_handle(handle, "attr1")
   assert(sdm.attr_drv_handle(sdm.driver_attached(handle), "attr1") ~= nil)
   assert(sdm.attr_dev_handle(handle, "attr1") ~= nil)
   assert(sdm.prvt_attr_handle(handle, "attr1") == nil)
   sdm.attr_copy(handle, "attr1")
   assert(sdm.attr_drv_handle(sdm.driver_attached(handle), "attr1") ~= nil)
   assert(sdm.attr_dev_handle(handle, "attr1") ~= nil)
   assert(sdm.prvt_attr_handle(handle, "attr1") ~= nil)
   local attr_cpy = sdm.attr_handle(handle, "attr1")
   assert(attr ~= nil)
   assert(attr_cpy ~= nil)
   assert(attr_cpy ~= attr)
   sdm.attr_set(attr_cpy, "string_copy")
   assert(sdm.attr_data(attr), "string")
   assert(sdm.attr_data(attr_cpy), "string_copy")
   local children = sdm.device_children(sdm.root())
   assert(sdm.device_name(children["onewire-1"]) == "onewire-1")
   assert(sdm.device_name(children["onewire-2"]) == "onewire-2")

   sdm.attr_copy(sdm.device_handle("onewire-1"), "attr1")
   sdm.attr_copy(sdm.device_handle("onewire-1"), "attr1")
   sdm.attr_copy(sdm.device_handle("onewire-1"), "attr2")
   sdm.attr_copy(sdm.device_handle("onewire-1"), "attr100")
   sdm.driver_release(sdm.device_handle("onewire-1"))

   assert(sdm.device_child(sdm.root(), "onewire-1") ~= nil)
   assert(sdm.device_name(sdm.device_child(sdm.root(), "onewire-1")) == "onewire-1")

   for i,j in pairs(sdm.devices()) do
      assert(sdm.device_name(j) == i)
   end

   local mtds = sdm.device_methods(handle)

   assert(mtds._poll ~= nil)
   assert(mtds._init ~= nil)
   assert(mtds.other == nil)
   assert(mtds._poll.func ~= nil)
   assert(mtds._init.func ~= nil)
   assert(mtds._poll.desc ~= nil)
   assert(mtds._init.desc == nil)

   assert(sdm.method_dev_handle(handle, "_poll") ~= nil)

   local attrs = sdm.device_prvt_attrs(handle)

   assert(attrs.attr1 ~= nil)
   assert(attrs.attr1.desc ~= nil)
   assert(attrs.attr2 == nil)
   assert(sdm.prvt_attr_handle(handle, "attr1") ~= nil)
   assert(sdm.prvt_attr_handle(handle, "attr2") == nil)
   assert(sdm.prvt_attr_remove(handle, sdm.prvt_attr_handle(handle, "attr1")))
   assert(sdm.prvt_attr_handle(handle, "attr1") == nil)

   assert(sdm.driver_attached(handle) == drv)
   assert(sdm.driver_release(handle))
   assert(sdm.driver_attached(handle) == nil)

   for name, handle in pairs(sdm.device_children(sdm.root())) do
      sdm.driver_release(handle)
      assert(sdm.device_remove(handle))
   end
   assert(#sdm.device_children(sdm.root()) == 0)
   assert(sdm.local_attr_add(sdm.root(), "id", "id", "123456", test_self, nil) ~= nil)
   assert(sdm.local_attr_add(sdm.root(), "id2", nil, 123456, nil, nil) ~= nil)
   assert(sdm.local_attr_handle(sdm.root(), "id") ~= nil)
   assert(sdm.driver_release(sdm.root()))
   assert(sdm.local_attr_handle(sdm.root(), "id") ~= nil)
   attrs = sdm.device_local_attrs(sdm.root())
   assert(attrs.id ~= nil)
   assert(attrs.id2 ~= nil)
   assert(attrs.id.desc ~= nil)
   assert(attrs.id2.desc == nil)
   sdm.local_attr_remove(sdm.root(), sdm.local_attr_handle(sdm.root(), "id"))
   assert(sdm.local_attr_handle(sdm.root(), "id") == nil)

   assert(sdm.device_add("test", sdm.root()) == nil)
   assert(sdm.device_poll(sdm.root()))
   assert(sdm.device_add("test", sdm.root()) ~= nil)
   assert(sdm.device_handle("test") ~= nil)
   local new_name = sdm.request_name("test")
   assert(new_name ~= nil)
   assert(sdm.device_rename(sdm.device_handle("test"), new_name) == true)
   assert(sdm.device_handle("test") == nil)
   assert(sdm.device_handle(new_name) ~= nil)
   sdm.destroy()
end

--[[ Device classes
   sdm.class_devices
--]]

do
   local function count_entries(t)
      i = 0
      for _,_ in pairs(t) do
	 i = i + 1
      end
      return i
   end

   local function NODEMCU_poll(dev)
      return sdm.device_name(dev) == "NODEMCU"
   end

   local function term_poll(dev, driver, parent)
      local name, instance = sdm.device_basename(dev)
      return name == "term"
   end

   local function time_poll(dev, driver, parent)
      local name, instance = sdm.device_basename(dev)
      return name == "time"
   end

   local function humid_poll(dev, driver, parent)
      local name, instance = sdm.device_basename(dev)
      return name == "humid"
   end

   local function hybrid_poll(dev, driver, parent)
      local name, instance = sdm.device_basename(dev)
      return name == "hybrid"
   end

   sdm.init()

   local drv = sdm.driver_add("NODEMCU", {"WIFI","TIME"})
   assert(drv ~= nil)
   assert(sdm.method_add(drv, "_poll", nil, NODEMCU_poll) ~= nil)
   assert(sdm.device_poll(sdm.root()))
   drv = sdm.driver_add("term", {"TERM"})
   assert(drv ~= nil)
   assert(sdm.method_add(drv, "_poll", nil, term_poll) ~= nil)
   drv = sdm.driver_add("time", {"TIME"})
   assert(drv ~= nil)
   assert(sdm.method_add(drv, "_poll", nil, time_poll) ~= nil)
   drv = sdm.driver_add("humid", {"HUMID"})
   assert(drv ~= nil)
   assert(sdm.method_add(drv, "_poll", nil, humid_poll) ~= nil)
   drv = sdm.driver_add("hybrid", {"HUMID", "TERM"})
   assert(drv ~= nil)
   assert(sdm.method_add(drv, "_poll", nil, hybrid_poll) ~= nil)

   for j, name in pairs({"term", "time", "humid", "hybrid"}) do
      for i=1,5 do
	 local dev = sdm.device_add(sdm.request_name(name), sdm.root())
	 assert(dev ~= nil)
	 assert(sdm.device_poll(dev))
      end
   end

   assert(count_entries(sdm.class_devices("WIF")) == 0)
   assert(count_entries(sdm.class_devices("WIFI")) == 1)
   assert(count_entries(sdm.class_devices("TERM")) == 10)
   assert(count_entries(sdm.class_devices("TIME")) == 6)
   assert(count_entries(sdm.class_devices("HUMID")) == 10)

   sdm.destroy()
end
