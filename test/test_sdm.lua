local sdm = require "sdm"

--[[ Random init
   sdm.init
   sdm.destroy
]]
do
   sdm.destroy()
   sdm.init()
   sdm.init()
   sdm.destroy()
   sdm.destroy()
   sdm.init()
   sdm.destroy()
   sdm.init()
   sdm.destroy()
end

--[[ Constants
   sdm.version
   sdm.pin_num
--]]

do
   assert(sdm.version ~= nil)
   assert(sdm.pin_num ~= nil)
end

-- Base device

do
   assert(sdm.device_handle("NODEMCU") == nil)
   sdm.init()
   assert(sdm.device_handle("NODEMCU") ~= nil)
   assert(sdm.device_handle() == nil)
   assert(sdm.device_handle("none") == nil)
   sdm.destroy()
   assert(sdm.device_handle() == nil)
   assert(sdm.device_handle("none") == nil)
end

--[[ Pin request test
   sdm.pin_request
   sdm.pin_device
   sdm.pin_free
]]

do
   sdm.init()
   local root = sdm.device_handle("NODEMCU")

   assert(sdm.pin_request(root, 0))
   assert(sdm.pin_request(root, 0) == false)
   assert(sdm.pin_request(root, 100) == false)
   assert(sdm.pin_device(0) ~= nil)
   assert(sdm.pin_device(0) == sdm.root())
   assert(sdm.pin_device(100) == nil)
   assert(sdm.pin_free(0))
   assert(sdm.pin_free(0) == false)
   assert(sdm.pin_device(0) == nil)
   assert(sdm.pin_free(100) == false)
   assert(sdm.pin_device(100) == nil)

   sdm.destroy()
end
