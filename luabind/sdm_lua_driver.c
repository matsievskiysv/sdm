
/**
 * @file sdm_lua_driver.c
 * @author Matsievskiy S.V.
 * @brief Lua driver bindings.
 * \par Webpage
 * <<https://gitlab.com/matsievskiysv/sdm>>
 *
 * \copyright (c) 2014 GNU GPL v3
 * \note This program is free software: you can redistribute it
 * and/or modify it under the terms of the
 * GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.
 * If not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA. 02111-1307, USA.
 * Or read it online at <<http://www.gnu.org/licenses/>>. *
 */

#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>

#include <sdm_driver.h>
#include <sdm_device.h>
#include <sdm_tree.h>

#include <sdm_lua_common.h>
#include <sdm_lua_driver.h>

int
lua_driver_add(lua_State * L)
{
	if (!lua_isstring(L, 1) || !lock_write())
		goto call_err;

	size_t name_sz = 0;
	const char* name = lua_tolstring(L, 1, &name_sz);
	size_t class_sz = 0;
	const char* class;

	if (lua_istable(L, 2)) {
		// concat class names into \n delimited string
		lua_table_concat(L, CLASSSPLITCHAR);
		class = lua_tolstring(L, -1, &class_sz);;
	}
	else {
		class = NULL;
	}

	struct driver* drv = driver_add(name, name_sz, class, class_sz);

	if (!drv)
		goto err;

	lua_pushlightuserdata(L, drv);
	unlock_write();
	return 1;

  err:
	unlock_write();
  call_err:
	lua_pushnil(L);
	return 1;
}

int
lua_driver_handle(lua_State * L)
{
	if (!lua_isstring(L, 1) || !lock_read())
		goto call_err;
	size_t size = 0;
	const char* name = lua_tolstring(L, 1, &size);
	struct driver* handle = driver_handle(name, size);

	if (!handle)
		goto err;

	lua_pushlightuserdata(L, handle);
	unlock_read();
	return 1;

  err:
	unlock_read();
  call_err:
	lua_pushnil(L);
	return 1;
}

int
lua_driver_name(lua_State * L)
{
	if (!lua_islightuserdata(L, 1) || !lock_read())
		goto call_err;

	struct driver* drv = lua_touserdata(L, 1);
	const struct str* str = driver_name(drv);

	if (!str)
		goto err;

	lua_pushlstring(L, str->string, str->size);
	unlock_read();
	return 1;

  err:
	unlock_read();
  call_err:
	lua_pushnil(L);
	return 1;
}

int
lua_driver_class(lua_State * L)
{
	if (!lua_islightuserdata(L, 1) || !lock_read())
		goto call_err;

	struct driver* drv = lua_touserdata(L, 1);
	const struct str* str = driver_class(drv);

	if (!str)
		goto err;

	lua_newtable(L);	// 1
	size_t i = 1, lp = 0;
	size_t p = chr_index(str, CLASSSPLITCHAR[0], i - 1);

	while (p != LLIST_ERR) {
		lua_pushinteger(L, (int) i++);	// 2
		lua_pushlstring(L, str->string + lp, p - lp);	// 3
		lua_settable(L, -3);	// 1[2] = 3
		lp = p + 1;
		p = chr_index(str, CLASSSPLITCHAR[0], i - 1);
	}
	lua_pushinteger(L, (int) i++);	// 2
	lua_pushlstring(L, str->string + lp, str->size - lp);	// 3
	lua_settable(L, -3);	// 1[2] = 3

	unlock_read();
	return 1;

  err:
	unlock_read();
  call_err:
	lua_pushnil(L);
	return 1;
}

int
lua_driver_remove(lua_State * L)
{
	if (!lua_islightuserdata(L, 1) || !lock_write())
		goto call_err;

	struct driver* drv = lua_touserdata(L, 1);

	if (!drv || (attached_count(drv) > 0))
		goto err;

	size_t met_cntr = 0;
	size_t att_cntr = 0;

	struct method* met = driver_method_next(drv, &met_cntr);

	while (met) {
		int f = method_func(met);

		if (f != -1)
			luaL_unref(L, LUA_REGISTRYINDEX, f);
		met = driver_method_next(drv, &met_cntr);
	}

	struct attr* att = driver_attr_next(drv, &att_cntr);

	while (att) {
		int g = attr_getter(att);

		if (g != -1)
			luaL_unref(L, LUA_REGISTRYINDEX, g);
		int s = attr_setter(att);

		if (s != -1)
			luaL_unref(L, LUA_REGISTRYINDEX, s);
		att = driver_attr_next(drv, &att_cntr);
	}
	lua_pushboolean(L, driver_remove(drv));
	unlock_write();
	return 1;

  err:
	unlock_write();
  call_err:
	lua_pushboolean(L, 0);
	return 1;
}

int
lua_method_drv_handle(lua_State * L)
{
	if (!lua_islightuserdata(L, 1) || !lua_isstring(L, 2) || !lock_read())
		goto call_err;
	struct driver* handle = lua_touserdata(L, 1);
	size_t size = 0;
	const char* name = lua_tolstring(L, 2, &size);
	struct method* met = method_drv_handle(handle, name, size);

	if (!met)
		goto err;

	lua_pushlightuserdata(L, met);
	unlock_read();
	return 1;

  err:
	unlock_read();
  call_err:
	lua_pushnil(L);
	return 1;
}

int
lua_method_add(lua_State * L)
{
	if (!lua_islightuserdata(L, 1) ||
	    !lua_isstring(L, 2) || !lua_isfunction(L, 4) || !lock_write())
		goto call_err;

	struct driver* handle = lua_touserdata(L, 1);
	size_t name_sz = 0;
	size_t desc_sz = 0;
	const char* name = lua_tolstring(L, 2, &name_sz);
	const char* desc = NULL;

	if (lua_isstring(L, 3))
		desc = lua_tolstring(L, 3, &desc_sz);

	int f = luaL_ref(L, LUA_REGISTRYINDEX);

	struct method* met =
		method_add(handle, name, name_sz, desc, desc_sz, f);
	if (!met) {
		luaL_unref(L, LUA_REGISTRYINDEX, f);
		goto err;
	}

	lua_pushlightuserdata(L, met);
	unlock_write();
	return 1;

  err:
	unlock_write();
  call_err:
	lua_pushnil(L);
	return 1;
}

int
lua_method_name(lua_State * L)
{
	if (!lua_islightuserdata(L, 1) || !lock_read())
		goto call_err;

	const struct str* str = method_name(lua_touserdata(L, 1));

	if (!str)
		goto err;

	lua_pushlstring(L, str->string, str->size);
	unlock_read();
	return 1;

  err:
	unlock_read();
  call_err:
	lua_pushnil(L);
	return 1;
}

int
lua_method_desc(lua_State * L)
{
	if (!lua_islightuserdata(L, 1) || !lock_read())
		goto call_err;

	const struct str* str = method_desc(lua_touserdata(L, 1));

	if (!str)
		goto err;

	lua_pushlstring(L, str->string, str->size);
	unlock_read();
	return 1;

  err:
	unlock_read();
  call_err:
	lua_pushnil(L);
	return 1;
}

int
lua_method_func(lua_State * L)
{
	if (!lua_islightuserdata(L, 1) || !lock_read())
		goto call_err;

	int f = method_func(lua_touserdata(L, 1));

	if (f == -1)
		goto err;

	lua_rawgeti(L, LUA_REGISTRYINDEX, f);
	unlock_read();
	return 1;

  err:
	unlock_read();
  call_err:
	lua_pushnil(L);
	return 1;
}

int
lua_method_remove(lua_State * L)
{
	if (!lua_islightuserdata(L, 1) || !lua_islightuserdata(L, 2) ||
	    !lock_write())
		goto call_err;

	struct driver* handle = lua_touserdata(L, 1);
	struct method* met = lua_touserdata(L, 2);
	int f = method_func(met);

	if (f != -1) {
		luaL_unref(L, LUA_REGISTRYINDEX, f);
	}

	lua_pushboolean(L, method_remove(handle, met));
	unlock_write();
	return 1;

  call_err:
	lua_pushboolean(L, 0);
	return 1;
}

int
lua_attr_add(lua_State * L)
{
	if (!lua_islightuserdata(L, 1) || !lua_isstring(L, 2) || !lock_write())
		goto call_err;
	struct driver* handle = lua_touserdata(L, 1);
	size_t name_sz = 0;
	size_t desc_sz = 0;
	const char* name = lua_tolstring(L, 2, &name_sz);
	const char* desc = NULL;

	if (lua_isstring(L, 3))
		desc = lua_tolstring(L, 3, &desc_sz);

	int get = -1, set = -1;
	struct attr* attribute = NULL;

	if (lua_isfunction(L, 6))
		set = luaL_ref(L, LUA_REGISTRYINDEX);
	else
		lua_pop(L, 1);

	if (lua_isfunction(L, 5))
		get = luaL_ref(L, LUA_REGISTRYINDEX);
	else
		lua_pop(L, 1);

	if (lua_isboolean(L, 4)) {
		int b = lua_toboolean(L, 4);

		attribute =
			attr_add_bool(handle, name, name_sz, desc, desc_sz,
				      b, get, set);
	}
	else if (lua_isnumber(L, 4)) {
		double n = lua_tonumber(L, 4);

		attribute =
			attr_add_num(handle, name, name_sz, desc, desc_sz, n,
				     get, set);
	}
	else if (lua_isstring(L, 4)) {
		size_t str_sz = 0;
		const char* str = lua_tolstring(L, 4, &str_sz);

		attribute =
			attr_add_str(handle, name, name_sz, desc, desc_sz,
				     str, str_sz, get, set);
	}
	else {
		goto attr_err;
	}

	if (!attribute)
		goto attr_err;

	// set function self values
	/*
	 * if (get != -1)
	 * lua_fenv_push(L, get, "self", 4, attribute);
	 * if (set != -1)
	 * lua_fenv_push(L, set, "self", 4, attribute);
	 */


	lua_pushlightuserdata(L, attribute);
	unlock_write();
	return 1;

  attr_err:
	if (get != -1) {
		luaL_unref(L, LUA_REGISTRYINDEX, get);
	}
	if (set != -1) {
		luaL_unref(L, LUA_REGISTRYINDEX, set);
	}
	unlock_write();
  call_err:
	lua_pushnil(L);
	return 1;
}

int
lua_attr_drv_handle(lua_State * L)
{
	if (!lua_islightuserdata(L, 1) || !lua_isstring(L, 2) || !lock_read())
		goto call_err;
	struct driver* handle = lua_touserdata(L, 1);
	size_t size = 0;
	const char* name = lua_tolstring(L, 2, &size);
	struct attr* att = attr_drv_handle(handle, name, size);

	if (!att)
		goto err;

	lua_pushlightuserdata(L, att);
	unlock_read();
	return 1;

  err:
	unlock_read();
  call_err:
	lua_pushnil(L);
	return 1;
}

int
lua_attr_set(lua_State * L)
{
	if (!lua_islightuserdata(L, 1) || !lock_write())
		goto call_err;

	struct attr* att = lua_touserdata(L, 1);

	switch (att->type) {
	case boolean:;
		{
			int d = lua_toboolean(L, 2);

			attr_set_bool(att, d);
		}
		break;
	case numeric:;
		{
			double d = lua_tonumber(L, 2);

			attr_set_num(att, d);
		}
		break;
	case string:;
		{
			size_t size = 0;
			const char* str = lua_tolstring(L, 2, &size);

			attr_set_str(att, str, size);
		}
		break;
	}

	lua_pushboolean(L, 0);
	unlock_write();
	return 1;

  call_err:
	lua_pushboolean(L, 0);
	return 1;
}

int
lua_attr_name(lua_State * L)
{
	if (!lua_islightuserdata(L, 1) || !lock_read())
		goto call_err;

	const struct str* str = attr_name(lua_touserdata(L, 1));

	if (!str)
		goto err;

	lua_pushlstring(L, str->string, str->size);
	unlock_read();
	return 1;

  err:
	unlock_read();
  call_err:
	lua_pushnil(L);
	return 1;
}

int
lua_attr_desc(lua_State * L)
{
	if (!lua_islightuserdata(L, 1) || !lock_read())
		goto call_err;

	const struct str* str = attr_desc(lua_touserdata(L, 1));

	if (!str)
		goto err;

	lua_pushlstring(L, str->string, str->size);
	unlock_read();
	return 1;

  err:
	unlock_read();
  call_err:
	lua_pushnil(L);
	return 1;
}

int
lua_attr_data(lua_State * L)
{
	if (!lua_islightuserdata(L, 1) || !lock_read())
		goto call_err;
	struct attr* att = lua_touserdata(L, 1);

	if (!chk_att(att))
		goto err;
	enum lua_types type = attr_type(att);
	union lua_type data = attr_data(att);

	switch (type) {
	case boolean:
		lua_pushboolean(L, data.boolean);
		break;
	case numeric:
		lua_pushnumber(L, data.numeric);
		break;
	case string:
		lua_pushlstring(L, data.string->string, data.string->size);
		break;
	}
	unlock_read();
	return 1;

  err:
	unlock_read();
  call_err:
	lua_pushnil(L);
	return 1;
}

int
lua_attr_func(lua_State * L)
{
	if (!lua_islightuserdata(L, 1) || !lock_read())
		goto call_err;
	struct attr* att = lua_touserdata(L, 1);
	int get = attr_getter(att);
	int set = attr_setter(att);

	if (get != -1) {
		lua_rawgeti(L, LUA_REGISTRYINDEX, get);
	}
	else {
		lua_pushnil(L);
	}
	if (set != -1) {
		lua_rawgeti(L, LUA_REGISTRYINDEX, set);
	}
	else {
		lua_pushnil(L);
	}
	unlock_read();
	return 2;

  call_err:
	lua_pushnil(L);
	lua_pushnil(L);
	return 2;
}

int
lua_attr_remove(lua_State * L)
{
	if (!lua_islightuserdata(L, 1) || !lua_islightuserdata(L, 2) ||
	    !lock_write())
		goto call_err;

	struct driver* handle = lua_touserdata(L, 1);
	struct attr* att = lua_touserdata(L, 2);
	int get = attr_getter(att);
	int set = attr_setter(att);

	if (get != -1) {

		luaL_unref(L, LUA_REGISTRYINDEX, get);
	}
	if (set != -1) {
		luaL_unref(L, LUA_REGISTRYINDEX, set);
	}
	lua_pushboolean(L, attr_remove(handle, att));
	unlock_write();
	return 1;

  call_err:
	lua_pushboolean(L, 0);
	return 1;
}

int
lua_drivers(lua_State * L)
{
	if (!lock_read())
		goto call_err;

	size_t count = driver_count();

	if ((count == LLIST_ERR) || (count == 0))
		goto err;

	lua_createtable(L, 0, (int) count);	// 1

	size_t drv_cntr = 0;

	while (drv_cntr != LLIST_ERR) {
		struct driver* drv = driver_next(&drv_cntr);

		if (!drv)
			continue;

		const struct str* name = driver_name(drv);

		lua_pushlstring(L, name->string, name->size);	// 2
		lua_pushlightuserdata(L, drv);	// 3
		lua_settable(L, -3);	// 1[2] = 3
	}
	unlock_read();
	return 1;
  err:
	unlock_read();
  call_err:
	lua_pushnil(L);
	return 1;
}

int
lua_driver_methods(lua_State * L)
{
	if (!lua_islightuserdata(L, 1) || !lock_read())
		goto call_err;

	struct driver* drv = lua_touserdata(L, 1);

	size_t count = method_count(drv);

	if ((count == LLIST_ERR) || (count == LLIST_ERR))
		goto err;

	lua_createtable(L, 0, (int) count);	// 1
	size_t met_cntr = 0;

	while (met_cntr != LLIST_ERR) {
		struct method* met = driver_method_next(drv, &met_cntr);

		if (!met)
			continue;

		int f = method_func(met);

		if (f < 0)
			continue;

		const struct str* name = method_name(met);
		const struct str* desc = method_desc(met);

		lua_pushlstring(L, name->string, name->size);	// 2
		lua_createtable(L, 0, 2 + (desc != NULL));	// 3
		if (desc) {
			lua_pushstring(L, "desc");	// 4
			lua_pushlstring(L, desc->string, desc->size);	// 5
			lua_settable(L, -3);	// 3[4] = 5
		}
		lua_pushstring(L, "func");	// 4
		lua_pushlightuserdata(L, met);	// 5
		lua_settable(L, -3);	// 3[4] = 5
		lua_pushstring(L, "handle");	// 4
		lua_pushlightuserdata(L, met);	// 5
		lua_settable(L, -3);	// 3[4] = 5
		lua_settable(L, -3);	// 1[2] = 3
	}
	unlock_read();
	return 1;

  err:
	unlock_read();
  call_err:
	lua_pushnil(L);
	return 1;
}

int
lua_driver_attrs(lua_State * L)
{
	if (!lua_islightuserdata(L, 1) || !lock_read())
		goto call_err;

	struct driver* drv = lua_touserdata(L, 1);

	size_t count_d = attr_count(drv);

	if ((count_d == LLIST_ERR) || (count_d == LLIST_ERR))
		goto err;

	lua_createtable(L, 0, (int) count_d);	// 1
	size_t att_cntr = 0;

	while (att_cntr != LLIST_ERR) {
		struct attr* attr = driver_attr_next(drv, &att_cntr);

		if (!attr)
			continue;

		int g = attr_getter(attr);
		int s = attr_setter(attr);

		const struct str* name = attr_name(attr);
		const struct str* desc = attr_desc(attr);

		lua_pushlstring(L, name->string, name->size);	// 2
		lua_createtable(L, 0, 1 + (g != -1) + (s != -1) + (desc != NULL));	// 3
		if (desc) {
			lua_pushstring(L, "desc");	// 4
			lua_pushlstring(L, desc->string, desc->size);	// 5
			lua_settable(L, -3);	// 3[4] = 5
		}
		if (g != -1) {
			lua_pushstring(L, "get");	// 4
			lua_rawgeti(L, LUA_REGISTRYINDEX, g);	// 5
			lua_settable(L, -3);	// 3[4] = 5
		}
		if (s != -1) {
			lua_pushstring(L, "set");	// 4
			lua_rawgeti(L, LUA_REGISTRYINDEX, s);	// 5
			lua_settable(L, -3);	// 3[4] = 5
		}
		lua_pushstring(L, "handle");	// 4
		lua_pushlightuserdata(L, attr);	// 5
		lua_settable(L, -3);	// 3[4] = 5
		lua_settable(L, -3);	// 1[2] = 3
	}
	unlock_read();
	return 1;

  err:
	unlock_read();
  call_err:
	lua_pushnil(L);
	return 1;
}
