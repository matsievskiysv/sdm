
/**
 * @file sdm_lua_common.h
 * @author Matsievskiy S.V.
 * @brief Lua library utility functions.
 * \par Webpage
 * <<https://gitlab.com/matsievskiysv/sdm>>
 *
 * \copyright (c) 2014 GNU GPL v3
 * \note This program is free software: you can redistribute it
 * and/or modify it under the terms of the
 * GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.
 * If not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA. 02111-1307, USA.
 * Or read it online at <<http://www.gnu.org/licenses/>>. *
 */

#pragma once

/**
 * \ingroup luaapi
 *
 * @{
 */

/** @brief Add lightuserdata to function environment.
 *
 * Lua binding for lua_fenv_push().
 * @param[in]	L lua stack.
 * @param[in]	func_ref reference to function.
 * @param[in]	val_name name of variable.
 * @param[in]	size size of \p val_name.
 * @param[in]	ptr lightuserdata.
 *
 * @retval 1 success.
 * @retval 0 error.
 */
int lua_fenv_push(lua_State * L, int func_ref, const char* val_name,
		  size_t size, void* ptr);

/**
 * \ingroup luaapi
 *
 * @{
 */

/** @brief Concat strings in table.
 *
 * Concat strings in table using function table.concat.
 * @param[in]	L lua stack with table on top.
 * @param[in]	ch concat char.
 *
 * @retval 0 success. Concat string replaced table on stack.
 * @retval LUA_ERRRUN a runtime error.
 * @retval LUA_ERRMEM memory allocation error.
 * @retval LUA_ERRERR error while running the error handler function
 */
int lua_table_concat(lua_State * L, const char* ch);

/**@}*/
