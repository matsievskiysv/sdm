
/**
 * @file sdm_lua_common.c
 * @author Matsievskiy S.V.
 * @brief Lua library utility functions.
 * \par Webpage
 * <<https://gitlab.com/matsievskiysv/sdm>>
 *
 * \copyright (c) 2014 GNU GPL v3
 * \note This program is free software: you can redistribute it
 * and/or modify it under the terms of the
 * GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.
 * If not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA. 02111-1307, USA.
 * Or read it online at <<http://www.gnu.org/licenses/>>. *
 */

#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>

#include <sdm_lua_common.h>

#include <sdm_funmap.h>

int
lua_fenv_push(lua_State * L, int func_ref, const char* val_name, size_t size,
	      void* ptr)
{
	lua_rawgeti(L, LUA_REGISTRYINDEX, func_ref);	// get function
	lua_getfenv(L, -1);	// push environment
	lua_pushlstring(L, val_name, size);	// push key
	lua_pushlightuserdata(L, ptr);	// push value
	lua_settable(L, -3);	// push key-value pair to table
	int rv = lua_setfenv(L, func_ref);	// set environment

	return rv;
}

int
lua_table_concat(lua_State * L, const char* ch)
{
	// assumed table on top of stack
	lua_getglobal(L, "table");
	lua_getfield(L, -1, "concat");
	lua_pushvalue(L, -3);	// copy table on top of the stack
	lua_remove(L, -3);	// remove original table
	lua_remove(L, -3);	// remove table lib
	lua_pushstring(L, ch);	// push delimited
	return lua_pcall(L, 2, 1, 0);	// concat string is on top of the stack now
}
