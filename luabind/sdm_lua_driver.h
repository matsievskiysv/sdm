
/**
 * @file sdm_lua_device.h
 * @author Matsievskiy S.V.
 * @brief Lua device bindings.
 * \par Webpage
 * <<https://gitlab.com/matsievskiysv/sdm>>
 *
 * \copyright (c) 2014 GNU GPL v3
 * \note This program is free software: you can redistribute it
 * and/or modify it under the terms of the
 * GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.
 * If not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA. 02111-1307, USA.
 * Or read it online at <<http://www.gnu.org/licenses/>>. *
 */

#pragma once

/**
 * \ingroup luaapi
 *
 * @{
 */

/** @brief Add driver.
 *
 * Lua binding for driver_add().
 * @param[in]	L lua stack.
 * - name of driver
 * - list of classes of driver (optional)
 *
 * @return number of returned arguments.
 * - new driver handle or nil
 */
int lua_driver_add(lua_State * L);

/** @brief Get driver handle.
 *
 * Lua binding for driver_handle().
 * @param[in]	L lua stack.
 * - name of driver
 *
 * @return number of returned arguments.
 * - new driver handle or nil
 */
int lua_driver_handle(lua_State * L);

/** @brief Get driver name.
 *
 * Lua binding for driver_name().
 * @param[in]	L lua stack.
 * - driver handle
 *
 * @return number of returned arguments.
 * - driver name or nil
 */
int lua_driver_name(lua_State * L);

/** @brief Get driver class.
 *
 * Lua binding for driver_name().
 * @param[in]	L lua stack.
 * - driver handle
 *
 * @return number of returned arguments.
 * - driver class list or nil
 */
int lua_driver_class(lua_State * L);

/** @brief Remove driver.
 *
 * Could only remove driver if it is not attached to any devices.
 * Lua binding for driver_remove().
 * @param[in]	L lua stack.
 * - #driver handle
 *
 * @return number of returned arguments.
 * - true: success or false: error
 */
int lua_driver_remove(lua_State * L);

/** @brief Add new method to driver.
 *
 * Lua binding for method_add().
 * @param[in]	L lua stack.
 * - driver handle
 * - method name
 * - method short description. nil for empty description
 * - method lua function
 *
 * @return number of returned arguments.
 * - new method handle or nil
 */
int lua_method_add(lua_State * L);

/** @brief Get driver method handle.
 *
 * Lua binding for method_drv_handle().
 * @param[in]	L lua stack.
 * - driver handle
 * - method name
 *
 * @return number of returned arguments.
 * - driver method handle or nil
 */
int lua_method_drv_handle(lua_State * L);

/** @brief Get method name.
 *
 * Lua binding for method_name().
 * @param[in]	L lua stack.
 * - method handle
 *
 * @return number of returned arguments.
 * - method name or nil
 */
int lua_method_name(lua_State * L);

/** @brief Get method description.
 *
 * Lua binding for method_desc().
 * @param[in]	L lua stack.
 * - method handle
 *
 * @return number of returned arguments.
 * - method description or nil
 */
int lua_method_desc(lua_State * L);

/** @brief Get method function.
 *
 * Lua binding for method_func().
 * @param[in]	L lua stack.
 * - method handle
 *
 * @return number of returned arguments.
 * - method function or nil
 */
int lua_method_func(lua_State * L);

/** @brief Remove method from driver.
 *
 * Lua binding for method_remove().
 * @param[in]	L lua stack.
 * - driver handle
 * - method handle
 *
 * @return number of returned arguments.
 * - true: success or false: error
 */
int lua_method_remove(lua_State * L);

/** @brief Add driver attribute.
 *
 * Lua binding for attr_add_bool(), attr_add_num()
 * and attr_add_str().
 * @param[in]	L lua stack.
 * - driver handle
 * - attribute name
 * - attribute short description
 * - attribute data. Boolean, numeric or string
 * - attribute getter function.
 * - attribute setter function.
 *
 * @return number of returned arguments.
 * - new attribute handle or nil
 */
int lua_attr_add(lua_State * L);

/** @brief Get driver attribute handle.
 *
 * Lua binding for attr_drv_handle().
 * @param[in]	L lua stack.
 * - driver handle
 * - attribute name
 *
 * @return number of returned arguments.
 * - driver attribute handle or nil
 */
int lua_attr_drv_handle(lua_State * L);

/** @brief Set attribute value.
 *
 * Lua binding for attr_set_bool(), attr_set_num()
 * and attr_set_str().
 * @param[in]	L lua stack.
 * - attribute handle
 * - attribute data. Boolean, numeric or string
 *
 * @return number of returned arguments.
 * - attribute handle or nil
 */
int lua_attr_set(lua_State * L);

/** @brief Get attribute name.
 *
 * Lua binding for attr_name().
 * @param[in]	L lua stack.
 * - attribute handle
 *
 * @return number of returned arguments.
 * - attribute name or nil
 */
int lua_attr_name(lua_State * L);

/** @brief Get attribute description.
 *
 * Lua binding for attr_desc().
 * @param[in]	L lua stack.
 * - attribute handle
 *
 * @return number of returned arguments.
 * - attribute description or nil
 */
int lua_attr_desc(lua_State * L);

/** @brief Get attribute data.
 *
 * Lua binding for attr_data().
 * @param[in]	L lua stack.
 * - attribute handle
 *
 * @return number of returned arguments.
 * - attribute data or nil
 */
int lua_attr_data(lua_State * L);

/** @brief Get attribute functions.
 *
 * Lua binding for attr_getter() and attr_setter().
 * @param[in]	L lua stack.
 * - attribute handle
 *
 * @return number of returned arguments.
 * - attribute getter function or nil
 * - attribute setter function or nil
 */
int lua_attr_func(lua_State * L);

/** @brief Remove attribute from driver.
 *
 * Lua binding for attr_remove().
 * @param[in]	L lua stack.
 * - driver handle
 * - attribute handle
 *
 * @return number of returned arguments.
 * - true: success or false: error
 */
int lua_attr_remove(lua_State * L);

/** @brief Get list of drivers.
 *
 * Get table of drivers.
 * Lua table syntax: {*name* = *handle*, ...}
 * @return number of returned arguments.
 * - table or nil
 */
int lua_drivers(lua_State * L);

/** @brief Get table of driver methods.
 *
 * Get table of driver methods.
 * Lua table syntax: {*methodname* = { [ desc= *description*,] func = *function*}, ...}
 * @param[in]	L lua stack.
 * - driver handle
 *
 * @return number of returned arguments.
 * - table or nil
 */
int lua_driver_methods(lua_State * L);

/** @brief Get table of driver attributes.
 *
 * Get table of driver attributes.
 * Lua table syntax: {*attrname* = { [ desc = *description* ,] [ get = *function*,] [ set= *function* ] }, ...}
 * @param[in]	L lua stack.
 * - driver handle
 *
 * @return number of returned arguments.
 * - table or nil
 */
int lua_driver_attrs(lua_State * L);

/**@}*/
