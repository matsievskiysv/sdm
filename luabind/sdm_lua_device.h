
/**
 * @file sdm_lua_device.c
 * @author Matsievskiy S.V.
 * @brief Lua device bindings.
 * \par Webpage
 * <<https://gitlab.com/matsievskiysv/sdm>>
 *
 * \copyright (c) 2014 GNU GPL v3
 * \note This program is free software: you can redistribute it
 * and/or modify it under the terms of the
 * GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.
 * If not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA. 02111-1307, USA.
 * Or read it online at <<http://www.gnu.org/licenses/>>. *
 */

#pragma once

/**
 * \ingroup luaapi
 *
 * @{
 */

/** @brief Add device.
 *
 * Lua binding for device_add().
 * @param[in]	L lua stack.
 * - name of device
 * - handle of parent
 *
 * @return number of returned arguments.
 * - new device handle or nil
 */
int lua_device_add(lua_State * L);

/** @brief Device handle.
 *
 * Lua binding for device_handle().
 * @param[in]	L lua stack.
 * - name of device
 *
 * @return number of returned arguments.
 * - device handle or nil
 */
int lua_device_handle(lua_State * L);

/** @brief Get #device parent.
 *
 * Lua binding for device_parent().
 * @param[in]	L lua stack.
 * - #device handle
 *
 * @return number of returned arguments.
 * - parent #device handle or nil
 */
int lua_device_parent(lua_State * L);

/** @brief Remove #device.
 *
 * Could only remove device if it does not have
 * children and not attached to driver.
 * Lua binding for device_remove().
 * @param[in]	L lua stack.
 * - #device handle
 *
 * @return number of returned arguments.
 * - true: success or false: error
 */
int lua_device_remove(lua_State * L);

/** @brief Device poll.
 *
 * Lua binding for device_poll().
 * Request #driver for #device.
 * @param[in]	L lua stack.
 * - #device handle
 *
 * @return number of returned arguments.
 * - true: success or false: error
 */
int lua_device_poll(lua_State * L);

/** @brief Get child of #device.
 *
 * Lua binding for device_child().
 * @param[in]	L lua stack.
 * - device handle
 * - child name
 *
 * @return number of returned arguments.
 * - child #device handle or nil
 */
int lua_device_child(lua_State * L);

/** @brief Change device name.
 *
 * Lua binding for device_rename().
 * Fails if name is already in use.
 * @param[in]	L lua stack.
 * - device handle
 * - new name
 *
 * @return number of returned arguments.
 * - true: success or false: error
 */
int lua_device_rename(lua_State * L);

/** @brief Get device name.
 *
 * Lua binding for device_name().
 * @param[in]	L lua stack.
 * - device handle
 *
 * @return number of returned arguments.
 * - device name or nil
 */
int lua_device_name(lua_State * L);

/** @brief Get device basename and suffix.
 *
 * Split string over #DEVSPLTCHAR character and return two parts.
 * @param[in]	L lua stack.
 * - device handle
 *
 * @return number of returned arguments.
 * - basename or nil
 * - suffix or nil
 */
int lua_device_basename(lua_State * L);

/** @brief Get #driver attached to #device.
 *
 * Lua binding for driver_attached().
 * @param[in]	L lua stack.
 * - device handle
 *
 * @return number of returned arguments.
 * - driver handle or nil
 */
int lua_driver_attached(lua_State * L);

/** @brief Release #device #driver.
 *
 * Could only release bus driver only if it has no children.
 * Lua binding for driver_release().
 * @param[in]	L lua stack.
 * - device handle
 *
 * @return number of returned arguments.
 * - true: success or false: error
 */
int lua_driver_release(lua_State * L);

/** @brief Get #method handle
 *
 * Lua binding for method_dev_handle().
 * @param[in]	L lua stack.
 * - device handle
 * - method name
 *
 * @return number of returned arguments.
 * - method handle or nil
 */
int lua_method_dev_handle(lua_State * L);

/** @brief Add local device attribute.
 *
 * Lua binding for local_attr_add_bool(), local_attr_add_num()
 * and local_attr_add_str().
 * @param[in]	L lua stack.
 * - device handle
 * - attribute name
 * - attribute short description. nil for empty description
 * - attribute data. Boolean, numeric or string
 * - attribute getter function
 * - attribute setter function
 *
 * @return number of returned arguments.
 * - new attribute handle or nil
 */
int lua_local_attr_add(lua_State * L);

/** @brief Get local device attribute handle.
 *
 * Lua binding for local_attr_handle().
 * @param[in]	L lua stack.
 * - device handle
 * - attribute name
 *
 * @return number of returned arguments.
 * - device attribute handle or nil
 */
int lua_local_attr_handle(lua_State * L);

/** @brief Remove local attribute from device.
 *
 * Lua binding for local_attr_remove().
 * @param[in]	L lua stack.
 * - device handle
 * - attribute handle
 *
 * @return number of returned arguments.
 * - true: success or false: error
 */
int lua_local_attr_remove(lua_State * L);

/** @brief Get #attr handle
 *
 * Lua binding for prvt_attr_handle().
 * @param[in]	L lua stack.
 * - device handle
 * - attr name
 *
 * @return number of returned arguments.
 * - attr handle or nil
 */
int lua_prvt_attr_handle(lua_State * L);

/** @brief Copy attribute to device
 *
 * Lua binding for attr_copy().
 * @param[in]	L lua stack.
 * - device handle
 * - attr name
 *
 * @return number of returned arguments.
 * - true: success or false: error
 */
int lua_attr_copy(lua_State * L);

/** @brief Remove private attribute from device.
 *
 * Lua binding for prvt_attr_remove().
 * @param[in]	L lua stack.
 * - device handle
 * - attribute handle
 *
 * @return number of returned arguments.
 * - true: success or false: error
 */
int lua_prvt_attr_remove(lua_State * L);

/** @brief Get driver #attr using #device handle
 *
 * Lua binding for attr_dev_handle().
 * @param[in]	L lua stack.
 * - device handle
 * - attr name
 *
 * @return number of returned arguments.
 * - attr handle or nil
 */
int lua_attr_dev_handle(lua_State * L);

/** @brief Get either private or driver #attr handle
 *
 * Try finding attribute using prvt_attr_handle().
 * If not found, try attr_drv_handle().
 * @param[in]	L lua stack.
 * - device handle
 * - attr name
 *
 * @return number of returned arguments.
 * - attr handle or nil
 */
int lua_attr_handle(lua_State * L);

/** @brief Get table of devices.
 *
 * Get table of devices.
 * Lua table syntax: { *device* = { *handle* }, ...}
 * @param[in]	L lua stack.
 *
 * @return number of returned arguments.
 * - table or nil
 */
int lua_devices(lua_State * L);

/** @brief Get table of devices of class.
 *
 * Get table of devices of class.
 * Lua table syntax: { *device* = { *handle* }, ...}
 * @param[in]	L lua stack.
 * - class name
 *
 * @return number of returned arguments.
 * - table or nil
 */
int lua_class_devices(lua_State * L);

/** @brief Get table of device methods.
 *
 * Get table of device methods.
 * Lua table syntax: { *methodname* = { [ desc = *description* ,] func = *function* }, ...}
 * @param[in]	L lua stack.
 * - device handle
 *
 * @return number of returned arguments.
 * - table or nil
 */
int lua_device_methods(lua_State * L);

/** @brief Get table of device local attributes.
 *
 * Get table of device local attributes.
 * Lua table syntax: { *attrname* = { [ desc = *description* ,] [ get = *function* ,] [ set = *function* ] }, ...}
 * @param[in]	L lua stack.
 * - device handle
 *
 * @return number of returned arguments.
 * - table or nil
 */
int lua_device_local_attrs(lua_State * L);

/** @brief Get table of device private attributes.
 *
 * Get table of device private attributes.
 * Lua table syntax: { *attrname* = { [ desc = *description* ,] [get = *function* ,] [ set = *function* ] }, ...}
 * @param[in]	L lua stack.
 * - device handle
 *
 * @return number of returned arguments.
 * - table or nil
 */
int lua_device_prvt_attrs(lua_State * L);

/** @brief Device children.
 *
 * Get list of #device children.
 * Lua table syntax: { *name* = *handle* , ...}
 * @param[in]	L lua stack.
 * - #device handle
 *
 * @return number of returned arguments.
 * - table or nil
 */
int lua_device_children(lua_State * L);

/**@}*/
