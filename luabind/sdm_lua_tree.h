
/**
 * @file sdm_lua_tree.h
 * @author Matsievskiy S.V.
 * @brief Lua tree bindings.
 * \par Webpage
 * <<https://gitlab.com/matsievskiysv/sdm>>
 *
 * \copyright (c) 2014 GNU GPL v3
 * \note This program is free software: you can redistribute it
 * and/or modify it under the terms of the
 * GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.
 * If not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA. 02111-1307, USA.
 * Or read it online at <<http://www.gnu.org/licenses/>>. *
 */

#pragma once

/**
 * \ingroup luaapi
 *
 * @{
 */

/** @brief Initialize library.
 *
 * Lua binding for tree_init().
 * @param[in]	L lua stack.
 */
int lua_init(lua_State * L);

/** @brief Destroy library data structures.
 *
 * Free #sdm tree and remove references to all lua functions.
 * Lua binding for tree_destroy().
 * @param[in]	L lua stack.
 */
int lua_destroy(lua_State * L);

/** @brief Remove unused drivers.
 *
 * @param[in]	L lua stack.
 *
 * @return number of returned arguments.
 * - true: success or false: error
 */
int lua_driver_prune(lua_State * L);

/** @brief Get #sdm root #device.
 *
 * Get #sdm root #device.
 * Lua binding for sdm_root().
 * @param[in]	L lua stack.
 *
 * @return number of returned arguments.
 * - root device handle or nil
 */
int lua_root(lua_State * L);

/** @brief Get type of handle.
 *
 * Lua binding for handle_type().
 * @param[in]	L lua stack.
 *
 * @return number of returned arguments.
 * - string "driver" or "device" or "method" or "attribute" or "unknown" or nil
 */
int lua_handle_type(lua_State * L);

/** @brief Associate pin with #device.
 *
 * Pin number range is [0,#PINNUM)
 * Lua binding for pin_request().
 * @param[in]	L lua stack.
 * - device handle
 * - pin number
 *
 * @return number of returned arguments.
 * - true: success or false: error.
 */
int lua_pin_request(lua_State * L);

/** @brief Free pin from #device.
 *
 * Pin number range is [0,#PINNUM)
 * Lua binding for pin_free().
 * @param[in]	L lua stack.
 * - pin number
 *
 * @return number of returned arguments.
 * - true: success or false: error
 */
int lua_pin_free(lua_State * L);

/** @brief Get #device, associated with pin.
 *
 * Pin number range is [0,#PINNUM)
 * Lua binding for pin_device().
 * @param[in]	L lua stack.
 * - pin number
 *
 * @return number of returned arguments.
 * - #device handle or nil
 */
int lua_pin_device(lua_State * L);

/** @brief Get avaliable device name given basename.
 *
 * @return number of returned arguments.
 * - avaliable #device name or nil
 */
int lua_request_name(lua_State * L);

/**@}*/
