
/**
 * @file sdm_lua_common.c
 * @author Matsievskiy S.V.
 * @brief Lua library methods table definition.
 * \par Webpage
 * <<https://gitlab.com/matsievskiysv/sdm>>
 *
 * \copyright (c) 2014 GNU GPL v3
 * \note This program is free software: you can redistribute it
 * and/or modify it under the terms of the
 * GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.
 * If not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA. 02111-1307, USA.
 * Or read it online at <<http://www.gnu.org/licenses/>>. *
 */

#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>

#include <sdm_common.h>
#include <sdm_lua_device.h>
#include <sdm_lua_driver.h>
#include <sdm_lua_tree.h>

#ifdef DESKTOPBUILD

#else

#include <module.h>

#endif

#ifndef LSTRKEY
#define LSTRKEY(x) x
#endif

#ifndef LFUNCVAL
#define LFUNCVAL(x) x
#endif

#ifndef LNUMVAL
#define LNUMVAL(x) x
#endif

#ifndef LNILKEY
#define LNILKEY NULL
#endif

#ifndef LNILVAL
#define LNILVAL NULL
#endif

#ifndef LUA_REG_TYPE
typedef struct luaL_Reg LUA_REG_TYPE;
#endif

#if defined(SDMSDK2) || defined(DESKTOPBUILD)

static const LUA_REG_TYPE sdm_map[] = {
	// lua_device.h
	{LSTRKEY("device_add"), LFUNCVAL(lua_device_add)},
	{LSTRKEY("device_handle"), LFUNCVAL(lua_device_handle)},
	{LSTRKEY("device_parent"), LFUNCVAL(lua_device_parent)},
	{LSTRKEY("device_remove"), LFUNCVAL(lua_device_remove)},
	{LSTRKEY("device_poll"), LFUNCVAL(lua_device_poll)},
	{LSTRKEY("device_child"), LFUNCVAL(lua_device_child)},
	{LSTRKEY("device_rename"), LFUNCVAL(lua_device_rename)},
	{LSTRKEY("device_name"), LFUNCVAL(lua_device_name)},
	{LSTRKEY("device_basename"), LFUNCVAL(lua_device_basename)},
	{LSTRKEY("driver_attached"), LFUNCVAL(lua_driver_attached)},
	{LSTRKEY("driver_release"), LFUNCVAL(lua_driver_release)},
	{LSTRKEY("method_dev_handle"), LFUNCVAL(lua_method_dev_handle)},
	{LSTRKEY("local_attr_add"), LFUNCVAL(lua_local_attr_add)},
	{LSTRKEY("local_attr_handle"), LFUNCVAL(lua_local_attr_handle)},
	{LSTRKEY("local_attr_remove"), LFUNCVAL(lua_local_attr_remove)},
	{LSTRKEY("prvt_attr_handle"), LFUNCVAL(lua_prvt_attr_handle)},
	{LSTRKEY("attr_copy"), LFUNCVAL(lua_attr_copy)},
	{LSTRKEY("prvt_attr_remove"), LFUNCVAL(lua_prvt_attr_remove)},
	{LSTRKEY("attr_dev_handle"), LFUNCVAL(lua_attr_dev_handle)},
	{LSTRKEY("attr_handle"), LFUNCVAL(lua_attr_handle)},
	{LSTRKEY("devices"), LFUNCVAL(lua_devices)},
	{LSTRKEY("class_devices"), LFUNCVAL(lua_class_devices)},
	{LSTRKEY("device_methods"), LFUNCVAL(lua_device_methods)},
	{LSTRKEY("device_local_attrs"), LFUNCVAL(lua_device_local_attrs)},
	{LSTRKEY("device_prvt_attrs"), LFUNCVAL(lua_device_prvt_attrs)},
	{LSTRKEY("device_children"), LFUNCVAL(lua_device_children)},
	// lua_driver.h
	{LSTRKEY("driver_add"), LFUNCVAL(lua_driver_add)},
	{LSTRKEY("driver_handle"), LFUNCVAL(lua_driver_handle)},
	{LSTRKEY("driver_name"), LFUNCVAL(lua_driver_name)},
	{LSTRKEY("driver_class"), LFUNCVAL(lua_driver_class)},
	{LSTRKEY("driver_remove"), LFUNCVAL(lua_driver_remove)},
	{LSTRKEY("method_add"), LFUNCVAL(lua_method_add)},
	{LSTRKEY("method_drv_handle"), LFUNCVAL(lua_method_drv_handle)},
	{LSTRKEY("method_name"), LFUNCVAL(lua_method_name)},
	{LSTRKEY("method_desc"), LFUNCVAL(lua_method_desc)},
	{LSTRKEY("method_func"), LFUNCVAL(lua_method_func)},
	{LSTRKEY("method_remove"), LFUNCVAL(lua_method_remove)},
	{LSTRKEY("attr_add"), LFUNCVAL(lua_attr_add)},
	{LSTRKEY("attr_drv_handle"), LFUNCVAL(lua_attr_drv_handle)},
	{LSTRKEY("attr_set"), LFUNCVAL(lua_attr_set)},
	{LSTRKEY("attr_name"), LFUNCVAL(lua_attr_name)},
	{LSTRKEY("attr_desc"), LFUNCVAL(lua_attr_desc)},
	{LSTRKEY("attr_data"), LFUNCVAL(lua_attr_data)},
	{LSTRKEY("attr_func"), LFUNCVAL(lua_attr_func)},
	{LSTRKEY("attr_remove"), LFUNCVAL(lua_attr_remove)},
	{LSTRKEY("drivers"), LFUNCVAL(lua_drivers)},
	{LSTRKEY("driver_methods"), LFUNCVAL(lua_driver_methods)},
	{LSTRKEY("driver_attrs"), LFUNCVAL(lua_driver_attrs)},
	// lua_tree.h
	{LSTRKEY("init"), LFUNCVAL(lua_init)},
	{LSTRKEY("destroy"), LFUNCVAL(lua_destroy)},
	{LSTRKEY("driver_prune"), LFUNCVAL(lua_driver_prune)},
	{LSTRKEY("root"), LFUNCVAL(lua_root)},
	{LSTRKEY("handle_type"), LFUNCVAL(lua_handle_type)},
	{LSTRKEY("pin_request"), LFUNCVAL(lua_pin_request)},
	{LSTRKEY("pin_free"), LFUNCVAL(lua_pin_free)},
	{LSTRKEY("pin_device"), LFUNCVAL(lua_pin_device)},
	{LSTRKEY("request_name"), LFUNCVAL(lua_request_name)},
	{LNILKEY, LNILVAL}
};

#else

LROT_BEGIN(sdm)
	// lua_device.h
	LROT_FUNCENTRY(device_add, lua_device_add)
	LROT_FUNCENTRY(device_handle, lua_device_handle)
	LROT_FUNCENTRY(device_parent, lua_device_parent)
	LROT_FUNCENTRY(device_remove, lua_device_remove)
	LROT_FUNCENTRY(device_poll, lua_device_poll)
	LROT_FUNCENTRY(device_child, lua_device_child)
	LROT_FUNCENTRY(device_rename, lua_device_rename)
	LROT_FUNCENTRY(device_name, lua_device_name)
	LROT_FUNCENTRY(device_basename, lua_device_basename)
	LROT_FUNCENTRY(driver_attached, lua_driver_attached)
	LROT_FUNCENTRY(driver_release, lua_driver_release)
	LROT_FUNCENTRY(method_dev_handle, lua_method_dev_handle)
	LROT_FUNCENTRY(local_attr_add, lua_local_attr_add)
	LROT_FUNCENTRY(local_attr_handle, lua_local_attr_handle)
	LROT_FUNCENTRY(local_attr_remove, lua_local_attr_remove)
	LROT_FUNCENTRY(prvt_attr_handle, lua_prvt_attr_handle)
	LROT_FUNCENTRY(attr_copy, lua_attr_copy)
	LROT_FUNCENTRY(prvt_attr_remove, lua_prvt_attr_remove)
	LROT_FUNCENTRY(attr_dev_handle, lua_attr_dev_handle)
	LROT_FUNCENTRY(attr_handle, lua_attr_handle)
	LROT_FUNCENTRY(devices, lua_devices)
	LROT_FUNCENTRY(class_devices, lua_class_devices)
	LROT_FUNCENTRY(device_methods, lua_device_methods)
	LROT_FUNCENTRY(device_local_attrs, lua_device_local_attrs)
	LROT_FUNCENTRY(device_prvt_attrs, lua_device_prvt_attrs)
	LROT_FUNCENTRY(device_children, lua_device_children)
	// lua_driver.h
	LROT_FUNCENTRY(driver_add, lua_driver_add)
	LROT_FUNCENTRY(driver_handle, lua_driver_handle)
	LROT_FUNCENTRY(driver_name, lua_driver_name)
	LROT_FUNCENTRY(driver_class, lua_driver_class)
	LROT_FUNCENTRY(driver_remove, lua_driver_remove)
	LROT_FUNCENTRY(method_add, lua_method_add)
	LROT_FUNCENTRY(method_drv_handle, lua_method_drv_handle)
	LROT_FUNCENTRY(method_name, lua_method_name)
	LROT_FUNCENTRY(method_desc, lua_method_desc)
	LROT_FUNCENTRY(method_func, lua_method_func)
	LROT_FUNCENTRY(method_remove, lua_method_remove)
	LROT_FUNCENTRY(attr_add, lua_attr_add)
	LROT_FUNCENTRY(attr_drv_handle, lua_attr_drv_handle)
	LROT_FUNCENTRY(attr_set, lua_attr_set)
	LROT_FUNCENTRY(attr_name, lua_attr_name)
	LROT_FUNCENTRY(attr_desc, lua_attr_desc)
	LROT_FUNCENTRY(attr_data, lua_attr_data)
	LROT_FUNCENTRY(attr_func, lua_attr_func)
	LROT_FUNCENTRY(attr_remove, lua_attr_remove)
	LROT_FUNCENTRY(drivers, lua_drivers)
	LROT_FUNCENTRY(driver_methods, lua_driver_methods)
	LROT_FUNCENTRY(driver_attrs, lua_driver_attrs)
	// lua_tree.h
	LROT_FUNCENTRY(init, lua_init)
	LROT_FUNCENTRY(destroy, lua_destroy)
	LROT_FUNCENTRY(driver_prune, lua_driver_prune)
	LROT_FUNCENTRY(root, lua_root)
	LROT_FUNCENTRY(handle_type, lua_handle_type)
	LROT_FUNCENTRY(pin_request, lua_pin_request)
	LROT_FUNCENTRY(pin_free, lua_pin_free)
	LROT_FUNCENTRY(pin_device, lua_pin_device)
	LROT_FUNCENTRY(request_name, lua_request_name)
	LROT_END(sdm, NULL, 0)
#endif
#ifdef DESKTOPBUILD
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmissing-prototypes"
     int
       luaopen_sdm(lua_State * L)
{
	luaL_register(L, "sdm", sdm_map);

	lua_pushstring(L, "version");
	lua_pushnumber(L, SDM_VERSION);
	lua_settable(L, -3);

	lua_pushstring(L, "pin_num");
	lua_pushnumber(L, PINNUM);
	lua_settable(L, -3);

	return 1;
}

#pragma GCC diagnostic pop

#else
#ifdef SDMSDK2
	NODEMCU_MODULE(SDM, "sdm", sdm_map, NULL);
#else
	NODEMCU_MODULE(SDM, "sdm", sdm, NULL);
#endif

#endif
