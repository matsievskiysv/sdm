# desktop Makefile
ifneq (,)
This makefile requires GNU Make.
endif

CC := gcc
LUA := lua5.1

DEBUG ?= no
WARNINGS := -Wall -Wextra -pedantic -Wshadow -Wpointer-arith -Wcast-align \
             -Wwrite-strings -Wmissing-prototypes -Wmissing-declarations \
             -Wredundant-decls -Wnested-externs -Winline -Wno-long-long \
             -Wuninitialized -Wconversion -Wstrict-prototypes -Werror

CFLAGS ?= -DDESKTOPBUILD -std=gnu11 -fPIC -ffunction-sections -fno-jump-tables -fdata-sections -Wpointer-arith -Wundef -Werror -Wl,-EL -fno-inline-functions -nostdlib $(WARNINGS)
ifneq ($(DEBUG),no)
CFLAGS += -ggdb -DDEBUG
else
CFLAGS += -O2
endif

VGFLAGS ?= --track-origins=yes --leak-check=full --quiet

LIBNAME := sdm.so
OBJDIR := build
LIBDIR := lib
AUXLIBDIR := auxlib
LUABINDDIR := luabind
DOCDIR := doc
TESTDIR := test

COLOR_RED := '\033[0;31m'
COLOR_NC := '\033[0m'

INCLUDES :=	-I $(AUXLIBDIR) \
		-I $(LIBDIR) \
		-I $(LUABINDDIR) \
		 $(shell pkg-config --libs --cflags $(LUA))

SOURCES :=	$(wildcard $(AUXLIBDIR)/*.c) \
		$(wildcard $(LIBDIR)/*.c) \
		$(wildcard $(LUABINDDIR)/*.c)

all: $(LIBNAME)

build_dir:
	@mkdir -p $(OBJDIR)

$(LIBNAME): build_dir $(patsubst %.c,%.o,$(SOURCES))
	$(CC) --shared -fPIC $(CFLAGS) $(wildcard $(OBJDIR)/*.o) -o $@

%.o: %.c
	$(CC) -c $(CFLAGS) $(INCLUDES) $< -o $(OBJDIR)/$(notdir $@)

distclean: clean
	rm -rf $(OBJDIR)
	rm -rf $(DOCDIR)
	rm -f $(LIBNAME)

clean:
	rm -rf build doc
	find . \( \
		-iname '*~' \
		-o -iname '*.o' \
		-o -iname '*.bak' \
		-o -iname '*.so' \
		\) -delete

test: all
	$(foreach test,\
		$(wildcard $(TESTDIR)/test*.lua),\
		echo $(COLOR_RED)Running test script $(test)$(COLOR_NC); \
		valgrind $(VGFLAGS) $(LUA) $(test);)

indent:
	indent  --ignore-profile \
		--gnu-style \
		--use-tabs \
		--indent-level 8 \
		--indent-label	2 \
		--line-length 79 \
		--start-left-side-of-comments \
		--no-space-after-function-call-names \
		--space-after-cast \
		--blank-lines-after-declarations \
		--blank-lines-after-procedures \
		--blank-lines-before-block-comments \
		--break-before-boolean-operator \
		--braces-after-func-def-line \
		--braces-on-if-line \
		--braces-on-struct-decl-line \
		--comment-delimiters-on-blank-lines \
		--space-after-cast \
		--pointer-align-left \
		--preserve-mtime \
		 $(shell find . -iname '*.c' -o  -iname '*.h')

doc: Doxyfile
	doxygen $<

.PHONY: doc indent test clean distclean build_dir all
