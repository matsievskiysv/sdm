
/**
 * @file sdm_llist.h
 * @author Matsievskiy S.V.
 * @brief Linked list implementation for byte arrays.
 * \par Webpage
 * <<https://gitlab.com/matsievskiysv/sdm>>
 *
 * \copyright (c) 2014 GNU GPL v3
 * \note This program is free software: you can redistribute it
 * and/or modify it under the terms of the
 * GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.
 * If not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA. 02111-1307, USA.
 * Or read it online at <<http://www.gnu.org/licenses/>>. *
 */

#pragma once

#include <stdint.h>
#include <stddef.h>

/**
 * \defgroup llist llist
 * @brief Implementation of the linked list for byte arrays.
 *
 * This implementation stores data in form of byte array and its size. #llist is a handle to the list and is supposed to be embedded into user structs.
 * @{
 */

#define LLIST_ERR SIZE_MAX /**< llist error value*/

/** @brief Node of llist.
 *
 * Node of the llist containing the data.
 */
struct llist_node {

	struct llist_node* next; /**< Pointer to the next node. */

	size_t size; /**< Size of node data. */

	char storage[1] __attribute__((aligned(__BIGGEST_ALIGNMENT__)));

								     /**< Node data. */
};

/** @brief Head of the llist.
 *
 * It is designed to be embedded into other structs.
 */
struct llist {

	size_t size; /**< Size of llist. */

	struct llist_node* head; /**< Pointer to the first llist node. */
};

/** @brief Push node to llist.
 *
 * @param[in]	self #llist handle.
 * @param[in]	data Pointer to data to be pushed.
 * @param[in]	size Size of \p data to be pushed.
 *
 * @return Pointer to a new node.
 * @retval NULL error.
 */
void* llist_push(struct llist* self, const void* data, size_t size);

/** @brief Push empty node to llist for later allocation.
 *
 * @param[in]	self #llist handle.
 * @param[in]	size Size of \p data to be pushed.
 *
 * @return Pointer to a new node.
 * @retval NULL error.
 */
void* llist_push_blank(struct llist* self, size_t size);

/** @brief Get node by index.
 *
 * Return node at index \p index.
 *
 * @param[in]	self llist handle.
 * @param[in]	index Index of node.
 *
 * @return Pointer to a node at \p index.
 * @retval NULL not found.
 */
void* llist_index(const struct llist* self, size_t index);

/** @brief Search for llist node.
 *
 * Apply function \p func to llist nodes and return pointer to first matching node.
 *
 * @param[in]	self llist handle.
 * @param[in]	search_func Function to apply to nodes.
 * Storage is a pointer to current node; size is a size of node data; data is an arbitrary data passed by user.
 * Function should return 1 for match and 0 for miss.
 * @param[in]	data User data passed to function.
 * @param[in]	size Size of user \p data passed to function.
 *
 * @return Index of the first matched node.
 * @retval #LLIST_ERR not found.
 */
size_t llist_search(const struct llist* self,
		    int (*search_func)(void* storage,
				       size_t storage_sz,
				       const void* data,
				       size_t data_sz),
		    const void* data, size_t size);

/** @brief Search for llist node by address.
 *
 * Get index of node with data address \p addr.
 *
 * @param[in]	self llist handle.
 * @param[in]	addr #llist_node storage address.
 *
 * @return Index of the matched node.
 * @retval #LLIST_ERR not found.
 */
size_t llist_search_addr(const struct llist* self, const void* addr);

/** @brief Size of llist.
 *
 * Get size of llist.
 *
 * @param[in]	self llist handle.
 *
 * @return Number of llist nodes.
 */
size_t llist_size(const struct llist* self);

/** @brief Delete llist node.
 *
 * @param[in]	self llist handle.
 * @param[in]	index Index of node.
 *
 * @retval 0 success.
 * @retval #LLIST_ERR error.
 */
size_t llist_delete(struct llist* self, size_t index);

/** @brief Map function.
 *
 * Apply function to each node of llist.
 *
 * @param[in]	self llist handle.
 * @param[in]	map_func Function to apply to nodes.
 * Storage is a pointer to current node; size is a size of node data; data is an arbitrary data passed by user.
 * @param[in]	data User data passed to function.
 * @param[in]	size Size of user \p data passed to function.
 */
void llist_map(const struct llist* self,
	       void (*map_func)(void* storage,
				size_t storage_sz,
				const void* data,
				size_t data_sz), const void* data,
	       size_t size);

/** @brief Free llist.
 *
 * Free llist and all it's nodes.
 *
 * @param[in]	self llist handle.
 */
void llist_free(struct llist* self);

/** @brief Create new llist.
 *
 * Create new llist structure. It is a linked list for arbitrary sized byte arrays.
 *
 * @return llist handle.
 */
struct llist* llist_new(void);

/**@}*/
