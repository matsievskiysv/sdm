
/**
 * @file sdm_funmap.h
 * @author Matsievskiy S.V.
 * @brief Standrad library function maps.
 * \par Webpage
 * <<https://gitlab.com/matsievskiysv/sdm>>
 *
 * \copyright (c) 2014 GNU GPL v3
 * \note This program is free software: you can redistribute it
 * and/or modify it under the terms of the
 * GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.
 * If not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA. 02111-1307, USA.
 * Or read it online at <<http://www.gnu.org/licenses/>>. *
 */

#include <stdlib.h>
#include <stdio.h>

#define sdm_malloc(x) calloc(1, x) /**< Allocation function. */
#define sdm_free(x) free(x) /**< Deallocation function. */

#if defined(DEBUG)
#define SDM_DEBUG_MSG(format, ...)				\
	fprintf(stderr, "%s:%d:%s(): "format"\n",		\
		__FILE__, __LINE__, __func__, ##__VA_ARGS__) \
		/**< Debug message. */
#else
#define SDM_DEBUG_MSG(format, ...) /**< Debug message. */
#endif
