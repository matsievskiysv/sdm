
/**
 * @file sdm_llist.c
 * @author Matsievskiy S.V.
 * @brief Linked list implementation for byte arrays.
 * \par Webpage
 * <<https://gitlab.com/matsievskiysv/sdm>>
 *
 * \copyright (c) 2014 GNU GPL v3
 * \note This program is free software: you can redistribute it
 * and/or modify it under the terms of the
 * GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.
 * If not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA. 02111-1307, USA.
 * Or read it online at <<http://www.gnu.org/licenses/>>. *
 */

#include <stdlib.h>
#include <string.h>
#include <sdm_llist.h>
#include <sdm_funmap.h>

static struct llist_node*
llist_allocate(const void* data, size_t size)
{
	struct llist_node* new_node =
		sdm_malloc(sizeof(struct llist_node) +
			   sizeof(char) * (size - 1));
	void* target = NULL;

	if (!new_node)
		goto err;
	new_node->size = size;
	target = new_node->storage;
	memcpy(target, data, size);

	return new_node;

  err:
	return NULL;
}

static struct llist_node*
llist_allocate_blank(size_t size)
{
	struct llist_node* new_node =
		sdm_malloc(sizeof(struct llist_node) +
			   sizeof(char) * (size - 1));
	void* target = NULL;

	if (!new_node)
		goto err;
	new_node->size = size;
	target = new_node->storage;
	memset(target, 0, size);

	return new_node;

  err:
	return NULL;
}

void*
llist_push(struct llist* self, const void* data, size_t size)
{
	if (!self)
		goto err;
	struct llist_node* new_node = llist_allocate(data, size);

	if (!new_node)
		goto err;
	if (self->head) {
		new_node->next = self->head;
		self->head = new_node;
	}
	else {
		self->head = new_node;
	}
	self->size++;
	return new_node->storage;

  err:
	return NULL;
}

void*
llist_push_blank(struct llist* self, size_t size)
{
	if (!self)
		goto err;
	struct llist_node* new_node = llist_allocate_blank(size);

	if (!new_node)
		goto err;
	if (self->head) {
		new_node->next = self->head;
		self->head = new_node;
	}
	else {
		self->head = new_node;
	}
	self->size++;
	return new_node->storage;

  err:
	return NULL;
}

void*
llist_index(const struct llist* self, size_t index)
{
	if (!self)
		goto err;
	struct llist_node* node = self->head;

	if (!node)
		goto err;
	for (size_t i = 0; i < index; i++) {
		node = node->next;
		if (!node)
			goto err;
	}
	return node->storage;

  err:
	return NULL;
}

size_t
llist_search(const struct llist* self,
	     int (*search_func)(void* storage,
				size_t storage_sz,
				const void* data,
				size_t data_sz), const void* data, size_t size)
{
	if(!self)
		goto err;
	struct llist_node* node = self->head;
	size_t i = 0;

	while (node) {
		if (search_func(node->storage, node->size, data, size))
			return i;
		node = node->next;
		i++;
	}

  err:
	return LLIST_ERR;
}

size_t
llist_search_addr(const struct llist* self, const void* addr)
{
	if (!self)
		goto err;
	struct llist_node* node = self->head;
	size_t i = 0;

	while (node) {
		void* strg_addr = node->storage;

		if (strg_addr == addr)
			return i;
		node = node->next;
		i++;
	}

  err:
	return LLIST_ERR;
}

size_t
llist_size(const struct llist* self)
{
	if (!self)
		goto err;
	return self->size;

  err:
	return LLIST_ERR;
}

size_t
llist_delete(struct llist* self, size_t index)
{
	if (!self)
		goto err;
	struct llist_node* node = self->head;
	struct llist_node* prev = NULL;

	if (!node)
		goto err;
	for (size_t i = 0; i < index; i++) {
		prev = node;
		node = node->next;
		if (!node)
			goto err;
	}
	if (node == self->head) {
		self->head = node->next;
	}
	if (prev) {
		prev->next = node->next;
	}
	sdm_free(node);
	self->size--;
	if (self->size <= 0)
		self->head = NULL;
	return 1;

  err:
	return LLIST_ERR;
}

void
llist_map(const struct llist* self,
	  void (*map_func)(void* storage,
			   size_t storage_sz,
			   const void* data,
			   size_t data_sz), const void* data, size_t size)
{
	if(!self)
		goto err;
	struct llist_node* node = self->head;

	while (node) {
		map_func(node->storage, node->size, data, size);
		node = node->next;
	}

  err:
	return;
}

void
llist_free(struct llist* self)
{
	if (!self)
		goto err;
	struct llist_node* next = self->head;

	while (next) {
		struct llist_node* current = next;

		next = current->next;
		sdm_free(current);
	}
	self->size = 0;
	self->head = NULL;

  err:
	return;
}

struct llist*
llist_new(void)
{
	struct llist* new = sdm_malloc(sizeof(struct llist));

	return new;
}
