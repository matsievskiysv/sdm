
/**
 * @file sdm_device.h
 * @author Matsievskiy S.V.
 * @brief Device methods.
 * \par Webpage
 * <<https://gitlab.com/matsievskiysv/sdm>>
 *
 * \copyright (c) 2014 GNU GPL v3
 * \note This program is free software: you can redistribute it
 * and/or modify it under the terms of the
 * GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.
 * If not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA. 02111-1307, USA.
 * Or read it online at <<http://www.gnu.org/licenses/>>. *
 */

#pragma once

#include <sdm_common.h>

/**
 * \ingroup capi
 *
 * @{
 */

/** @brief Add #device.
 *
 * @param[in]	name Name of the new #device.
 * @param[in]	name_sz Size of \p name.
 * @param[in,out]	parent Parent of the new #device.
 *
 * @return Pointer to new #device.
 * @retval NULL error.
 */
struct device* device_add(const char* name,
			  size_t name_sz, struct device* parent);

/** @brief Get number of devices.
 *
 * @return Number of devices.
 * @retval LLIST_ERR error.
 */
size_t device_count(void);

/** @brief Get number of #device children.
 *
 * @param[in]	dev #device handle.
 *
 * @return Number of children.
 * @retval LLIST_ERR error.
 */
size_t children_count(const struct device* dev);

/** @brief Remove #device.
 *
 * @param[in]	dev #device to remove.
 *
 * @retval 1 success.
 * @retval 0 error.
 */
int device_remove(struct device* dev);

/** @brief Assign #driver to #device.
 *
 * This function assigns #driver to #device and enables #device.
 * Driver-less #device cannot be used and it cannot have children.
 * @param[in]	dev #device handle.
 * @param[in]	drv #driver to bind to #device.
 *
 * @retval 1 success.
 * @retval 0 error.
 */
int driver_assign(struct device* dev, struct driver* drv);

/** @brief Get #device #driver.
 *
 * Get #driver attached to the #device.
 * @param[in]	dev #device handle.
 *
 * @return pointer to #driver.
 * @retval NULL error.
 */
struct driver* driver_attached(const struct device* dev);

/** @brief Remove #driver from #device.
 *
 * @param[in]	dev #device handle.
 *
 * @retval 1 success.
 * @retval 0 error.
 */
int driver_release(struct device* dev);

/** @brief #device handle.
 *
 * @param[in]	name name of device.
 * @param[in]	size \p name size.
 *
 * @return #driver handle.
 * @retval NULL error.
 */
void* device_handle(const char* name, size_t size);

/** @brief #device parent.
 *
 * @param[in]	dev #device handle.
 *
 * @return #driver handle.
 * @retval NULL error.
 */
struct device* device_parent(const struct device* dev);

/** @brief #device child.
 *
 * @param[in]	dev #device handle.
 * @param[in]	name Child name.
 * @param[in]	name_sz \p name size.
 *
 * @return #driver handle.
 * @retval NULL error.
 */
struct device* device_child(const struct device* dev,
			    const char* name, size_t name_sz);

/** @brief #device rename.
 *
 * @param[in]	dev #device handle.
 * @param[in]	name name of #device.
 * @param[in]	name_sz \p name size.
 *
 * @return name of the #device.
 * @retval NULL error.
 */
int device_rename(struct device* dev, const char* name, size_t name_sz);

/** @brief #device name.
 *
 * @param[in]	dev #device handle.
 *
 * @return name of the #device.
 * @retval NULL error.
 */
const struct str* device_name(const struct device* dev);

/** @brief #method handle from #device.
 *
 * @param[in]	dev #device handle.
 * @param[in]	name name of #method.
 * @param[in]	name_sz \p name size.
 *
 * @return #method handle.
 * @retval NULL error.
 */
struct method* method_dev_handle(const struct device* dev,
				 const char* name, size_t name_sz);

/** @brief Get local #attr handle from #device.
 *
 * @param[in]	dev #device handle.
 * @param[in]	name Name of the method.
 * @param[in]	name_sz Size of \p name.
 *
 * @return #attr handle.
 * @retval NULL error.
 */
struct attr* local_attr_handle(struct device* dev,
			       const char* name, size_t name_sz);

/** @brief Add local boolean attribute to #device.
 *
 * @see https://www.lua.org/manual/5.1/manual.html#luaL_ref
 * @param[in,out]	dev #device handle.
 * @param[in]	name Name of the attribute.
 * @param[in]	name_sz Size of the \p name.
 * @param[in]	desc Short description of the attribute.
 * @param[in]	desc_sz Size of the \p desc.
 * @param[in]	data Attribute data
 * @param[in]	get Reference to getter lua function.
 * @param[in]	set Reference to setter lua function.
 *
 * @return New #attr handle.
 * @retval NULL error.
 */
struct attr* local_attr_add_bool(struct device* dev,
				 const char* name,
				 size_t name_sz,
				 const char* desc,
				 size_t desc_sz, int data, int get, int set);

/** @brief Add local numeric attribute to #device.
 *
 * @see https://www.lua.org/manual/5.1/manual.html#luaL_ref
 * @param[in,out]	dev #device handle.
 * @param[in]	name Name of the attribute.
 * @param[in]	name_sz Size of the \p name.
 * @param[in]	desc Short description of the attribute.
 * @param[in]	desc_sz Size of the \p desc.
 * @param[in]	data Attribute data.
 * @param[in]	get Reference to getter lua function.
 * @param[in]	set Reference to setter lua function.
 *
 * @return New #attr handle.
 * @retval NULL error.
 */
struct attr* local_attr_add_num(struct device* dev,
				const char* name,
				size_t name_sz,
				const char* desc,
				size_t desc_sz, double data, int get, int set);

/** @brief Add local string attribute to #device.
 *
 * @see https://www.lua.org/manual/5.1/manual.html#luaL_ref
 * @param[in,out]	dev #device handle.
 * @param[in]	name Name of the attribute.
 * @param[in]	name_sz Size of the \p name.
 * @param[in]	desc Short description of the attribute.
 * @param[in]	desc_sz Size of the \p desc.
 * @param[in]	data Attribute data.
 * @param[in]	data_sz Size of the \p data.
 * @param[in]	get Reference to getter lua function.
 * @param[in]	set Reference to setter lua function.
 *
 * @return New #attr handle.
 * @retval NULL error.
 */
struct attr* local_attr_add_str(struct device* dev,
				const char* name,
				size_t name_sz,
				const char* desc,
				size_t desc_sz,
				const char* data, size_t data_sz, int get,
				int set);

/** @brief Remove local #attr from #device.
 *
 * @param[in,out]	dev #device handle.
 * @param[in]	attr #attr handle.
 *
 * @retval 1 success.
 * @retval 0 error.
 */
int local_attr_remove(struct device* dev, struct attr* attr);

/** @brief Local #attr count of #device.
 *
 * Get number of local attributes of the given device.
 * @param[in]	dev #driver handle.
 *
 * @return Number of device local attributes.
 * @retval #LLIST_ERR driver not found.
 */
size_t attr_local_count(const struct device* dev);

/** @brief Get private #attr handle from #device.
 *
 * @param[in]	dev #device handle.
 * @param[in]	name name of #attr.
 * @param[in]	name_sz \p name size.
 *
 * @return #attr handle.
 * @retval NULL error.
 */
struct attr* prvt_attr_handle(struct device* dev,
			      const char* name, size_t name_sz);

/** @brief Copy #driver attribute to #device private attribute list.
 *
 * @param[in]	dev #device handle.
 * @param[in]	name name of #attr.
 * @param[in]	name_sz \p name size.
 *
 * @return copied #attr handle.
 * @retval NULL error.
 */
struct attr* attr_copy(struct device* dev, const char* name, size_t name_sz);

/** @brief Remove private #attr from #device.
 *
 * @param[in,out]	dev #device handle.
 * @param[in]	attr #attr handle.
 *
 * @retval 1 success.
 * @retval 0 error.
 */
int prvt_attr_remove(struct device* dev, struct attr* attr);

/** @brief Private #attr count of #device.
 *
 * @param[in]	dev #driver handle.
 *
 * @return Number of device private attributes.
 * @retval #LLIST_ERR driver not found.
 */
size_t attr_prvt_count(const struct device* dev);

/** @brief Get #attr handle from #device #driver
 *
 * @param[in]	dev #device handle.
 * @param[in]	name name of #attr.
 * @param[in]	name_sz \p name size.
 *
 * @return #attr handle.
 * @retval NULL error.
 */
struct attr* attr_dev_handle(struct device* dev,
			     const char* name, size_t name_sz);

/**@}*/
