
/**
 * @file sdm_driver.c
 * @author Matsievskiy S.V.
 * @brief Driver methods.
 * \par Webpage
 * <<https://gitlab.com/matsievskiysv/sdm>>
 *
 * \copyright (c) 2014 GNU GPL v3
 * \note This program is free software: you can redistribute it
 * and/or modify it under the terms of the
 * GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.
 * If not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA. 02111-1307, USA.
 * Or read it online at <<http://www.gnu.org/licenses/>>. *
 */

#pragma once

#include <sdm_common.h>

/** \ingroup  capi
 *
 * @{
 */

/** @brief Add #driver.
 *
 * @param[in]	name Name of the #driver to add.
 * @param[in]	name_sz Size of \p name.
 * @param[in]	class Class of the #driver new driver.
 * @param[in]	class_sz Size of \p class.
 *
 * @return New #driver handle.
 * @retval NULL error.
 */
struct driver* driver_add(const char* name, size_t name_sz,
			  const char* class, size_t class_sz);

/** @brief #driver handle by name.
 *
 * @param[in]	name Name of the #driver to add.
 * @param[in]	size Size of \p name.
 *
 * @return #driver handle.
 * @retval NULL error.
 */
struct driver* driver_handle(const char* name, size_t size);

/** @brief #driver index.
 *
 * Get index of #driver in #sdm device list.
 * @param[in]	drv #driver handle.
 *
 * @return Index of driver in #sdm list.
 * @retval #LLIST_ERR not found.
 */
size_t driver_index(const struct driver* drv);

/** @brief #driver name.
 *
 * @param[in]	drv Name of the #driver.
 *
 * @return #driver name.
 * @retval NULL error.
 */
const struct str* driver_name(const struct driver* drv);

/** @brief #driver class.
 *
 * @param[in]	drv Class of the #driver.
 *
 * @return #driver class.
 * @retval NULL error or none.
 */
const struct str* driver_class(const struct driver* drv);

/** @brief #driver remove.
 *
 * @param[in]	drv #driver handle.
 *
 * @retval 1 success.
 * @retval 0 error.
 */
int driver_remove(struct driver* drv);

/** @brief Add #method to #driver.
 *
 * Add method to #driver. Method is a reference to lua function.
 * @see https://www.lua.org/manual/5.1/manual.html#luaL_ref
 * @param[in,out]	drv #driver handle.
 * @param[in]	name Name of the method.
 * @param[in]	name_sz Size of \p name.
 * @param[in]	desc Short description of the method.
 * @param[in]	desc_sz Size of \p desc.
 * @param[in]	func Reference to lua function.
 *
 * @return new #method handle.
 * @retval NULL error.
 */
struct method* method_add(struct driver* drv,
			  const char* name,
			  size_t name_sz,
			  const char* desc, size_t desc_sz, int func);

/** @brief Get #method handle from #driver.
 *
 * @param[in]	drv #driver handle.
 * @param[in]	name Name of the method.
 * @param[in]	name_sz Size of \p name.
 *
 * @return #method handle.
 * @retval NULL error.
 */
struct method* method_drv_handle(struct driver* drv,
				 const char* name, size_t name_sz);

/** @brief Get number of registered drivers.
 *
 * @return Number of drivers.
 * @retval #LLIST_ERR driver not found.
 */
size_t driver_count(void);

/** @brief #method count of #driver.
 *
 * @param[in]	drv #driver handle.
 *
 * @return Number of driver methods.
 * @retval #LLIST_ERR driver not found.
 */
size_t method_count(const struct driver* drv);

/** @brief #method name.
 *
 * @param[in]	met #method handle.
 *
 * @return Name of the #method.
 * @retval NULL error.
 */
const struct str* method_name(const struct method* met);

/** @brief #method description.
 *
 * @param[in]	met #method handle.
 *
 * @return Name of the #method.
 * @retval NULL error.
 */
const struct str* method_desc(const struct method* met);

/** @brief Retrieve #method.
 *
 * Retrieve reference to lua function.
 * @see https://www.lua.org/manual/5.1/manual.html#luaL_ref
 * @param[in]	met #method handle.
 *
 * @return Reference to lua function.
 * @retval -1 error.
 */
int method_func(const struct method* met);

/** @brief Remove #method from #driver.
 *
 * @param[in,out]	drv #driver handle.
 * @param[in]	met #method handle.
 *
 * @retval 1 success.
 * @retval 0 error.
 */
int method_remove(struct driver* drv, struct method* met);

/** @brief Add boolean attribute to #driver.
 *
 * @see https://www.lua.org/manual/5.1/manual.html#luaL_ref
 * @param[in,out]	drv #driver handle.
 * @param[in]	name Name of the attribute.
 * @param[in]	name_sz Size of the \p name.
 * @param[in]	desc Short description of the attribute.
 * @param[in]	desc_sz Size of the \p desc.
 * @param[in]	data Attribute data
 * @param[in]	get Reference to getter lua function.
 * @param[in]	set Reference to setter lua function.
 *
 * @return New #attr handle.
 * @retval NULL error.
 */
struct attr* attr_add_bool(struct driver* drv,
			   const char* name,
			   size_t name_sz,
			   const char* desc,
			   size_t desc_sz, int data, int get, int set);

/** @brief Add numeric attribute to #driver.
 *
 * @see https://www.lua.org/manual/5.1/manual.html#luaL_ref
 * @param[in,out]	drv #driver handle.
 * @param[in]	name Name of the attribute.
 * @param[in]	name_sz Size of the \p name.
 * @param[in]	desc Short description of the attribute.
 * @param[in]	desc_sz Size of the \p desc.
 * @param[in]	data Attribute data.
 * @param[in]	get Reference to getter lua function.
 * @param[in]	set Reference to setter lua function.
 *
 * @return New #attr handle.
 * @retval NULL error.
 */
struct attr* attr_add_num(struct driver* drv,
			  const char* name,
			  size_t name_sz,
			  const char* desc,
			  size_t desc_sz, double data, int get, int set);

/** @brief Add string attribute to #driver.
 *
 * @see https://www.lua.org/manual/5.1/manual.html#luaL_ref
 * @param[in,out]	drv #driver handle.
 * @param[in]	name Name of the attribute.
 * @param[in]	name_sz Size of the \p name.
 * @param[in]	desc Short description of the attribute.
 * @param[in]	desc_sz Size of the \p desc.
 * @param[in]	data Attribute data.
 * @param[in]	data_sz Size of the \p data.
 * @param[in]	get Reference to getter lua function.
 * @param[in]	set Reference to setter lua function.
 *
 * @return New #attr handle.
 * @retval NULL error.
 */
struct attr* attr_add_str(struct driver* drv,
			  const char* name,
			  size_t name_sz,
			  const char* desc,
			  size_t desc_sz,
			  const char* data, size_t data_sz, int get, int set);

/** @brief Get #attr handle from #driver.
 *
 * @param[in]	drv #driver handle.
 * @param[in]	name Name of the method.
 * @param[in]	name_sz Size of \p name.
 *
 * @return #attr handle.
 * @retval NULL error.
 */
struct attr* attr_drv_handle(struct driver* drv,
			     const char* name, size_t name_sz);

/** @brief #attr type.
 *
 * @param[in]	attr #attr handle.
 *
 * @return Type of #attr.
 */
enum lua_types attr_type(const struct attr* attr);

/** @brief #attr data.
 *
 * @param[in]	attr #attr handle.
 *
 * @return #attr data.
 * @retval NULL error.
 */
union lua_type attr_data(const struct attr* attr);

/** @brief #attr name.
 *
 * @param[in]	attr #attr handle.
 *
 * @return Name of the #attr.
 * @retval NULL error.
 */
const struct str* attr_name(const struct attr* attr);

/** @brief #attr description.
 *
 * @param[in]	attr #attr handle.
 *
 * @return Description of the #attr.
 * @retval NULL error.
 */
const struct str* attr_desc(const struct attr* attr);

/** @brief Set new boolean value of #attr.
 *
 * @param[in,out]	attr #attr handle.
 * @param[in]	data New value.
 *
 * @retval 1 success.
 * @retval 0 error.
 */
int attr_set_bool(struct attr* attr, int data);

/** @brief Set new numeric value of #attr.
 *
 * @param[in,out]	attr #attr handle.
 * @param[in]	data New value.
 *
 * @retval 1 success.
 * @retval 0 error.
 */
int attr_set_num(struct attr* attr, double data);

/** @brief Set new string value of #attr.
 *
 * @param[in,out]	attr #attr handle.
 * @param[in]	data New value.
 * @param[in]	size Size of \p data.
 *
 * @retval 1 success.
 * @retval 0 error.
 */
int attr_set_str(struct attr* attr, const char* data, size_t size);

/** @brief Set new string value of #attr.
 *
 * Set new string value of #attr without freeing previously assigned one.
 * This function is needed for cloning strings in attr_copy() function.
 * @param[in,out]	attr #attr handle.
 * @param[in]	data New value.
 * @param[in]	size Size of \p data.
 *
 * @retval 1 success.
 * @retval 0 error.
 */
int attr_deref_str(struct attr* attr, const char* data, size_t size);

/** @brief Retrieve getter of #attr.
 *
 * Retrieve reference to lua getter function.
 * @see https://www.lua.org/manual/5.1/manual.html#luaL_ref
 * @param[in]	attr #attr handle.
 *
 * @return Reference to lua getter function.
 * @retval -1 error.
 */
int attr_getter(const struct attr* attr);

/** @brief Retrieve setter of #attr.
 *
 * Retrieve reference to lua setter function.
 * @see https://www.lua.org/manual/5.1/manual.html#luaL_ref
 * @param[in]	attr #attr handle.
 *
 * @return Reference to lua setter function.
 * @retval -1 error.
 */
int attr_setter(const struct attr* attr);

/** @brief Remove #attr from #driver.
 *
 * @param[in,out]	drv #driver handle.
 * @param[in]	attr #attr handle.
 *
 * @retval 1 success.
 * @retval 0 error.
 */
int attr_remove(struct driver* drv, struct attr* attr);

/** @brief #attr count of #driver.
 *
 * @param[in]	drv #driver handle.
 *
 * @return Number of driver attributes.
 * @retval #LLIST_ERR driver not found.
 */
size_t attr_count(const struct driver* drv);

/** @brief Count of devices attached to #driver.
 *
 * @param[in]	drv #driver handle.
 *
 * @return Number of attached devices.
 * @retval #LLIST_ERR driver not found.
 */
size_t attached_count(const struct driver* drv);

/**@}*/
