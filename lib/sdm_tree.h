
/**
 * @file sdm_tree.h
 * @author Matsievskiy S.V.
 * @brief Tree related functions.
 * \par Webpage
 * <<https://gitlab.com/matsievskiysv/sdm>>
 *
 * \copyright (c) 2014 GNU GPL v3
 * \note This program is free software: you can redistribute it
 * and/or modify it under the terms of the
 * GNU General Public License as published by
 * the Free Software Foundation version 3 of the License.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.
 * If not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA. 02111-1307, USA.
 * Or read it online at <<http://www.gnu.org/licenses/>>. *
 */

#pragma once

#include <sdm_common.h>

/** \ingroup capi
 *
 * @{
 */

/** @brief Initialize #sdm.
 *
 * Initialize #sdm structure.
 * @return #sdm root bus.
 * @retval NULL error.
 */
struct device* tree_init(void);

/** @brief Free #sdm structures.
 *
 * Free #sdm structure.
 */
void tree_destroy(void);

/** @brief Get #sdm root #device handle.
 *
 * Get #sdm root #device handle.
 * @return Root #device handle.
 * @retval NULL not found.
 */
struct device* sdm_root(void);

/** @brief #driver iterator get next.
 *
 * Get next #driver in #sdm driver list.
 * @param[in,out]	iter Iterator structure.
 *
 * @return #driver handle.
 * @retval NULL not found.
 */
struct driver* driver_next(size_t* iter);

/** @brief #device iterator get next.
 *
 * Get next #device in #sdm device list.
 * @param[in,out]	iter Iterator structure.
 *
 * @return #device handle.
 * @retval NULL not found.
 */
struct device* device_next(size_t* iter);

/** @brief #method iterator get next.
 *
 * Get next #method for #driver.
 * @param[in]	drv #driver handle.
 * @param[in,out]	iter Iterator structure.
 *
 * @return #method handle.
 * @retval NULL not found.
 */
struct method* driver_method_next(const struct driver* drv, size_t* iter);

/** @brief #attr iterator get next.
 *
 * Get next #attr for #driver.
 * @param[in]	drv #driver handle.
 * @param[in,out]	iter Iterator structure.
 *
 * @return #attr handle.
 * @retval NULL not found.
 */
struct attr* driver_attr_next(const struct driver* drv, size_t* iter);

/** @brief #attr iterator get next.
 *
 * Get next #attr for #device.
 * @param[in]	dev #device handle.
 * @param[in,out]	iter Iterator structure.
 *
 * @return #attr handle.
 * @retval NULL not found.
 */
struct attr* device_prvt_attr_next(const struct device* dev, size_t* iter);

/** @brief Local #attr iterator get next.
 *
 * @param[in]	dev #device handle.
 * @param[in,out]	iter Iterator structure.
 *
 * @return Local #attr handle.
 * @retval NULL not found.
 */
struct attr* device_local_attr_next(const struct device* dev, size_t* iter);

/** @brief #device children iterator.
 *
 * Get next child #device of #device.
 * @param[in]	dev #device handle.
 * @param[in,out]	iter Iterator structure.
 *
 * @return #device handle.
 * @retval NULL not found.
 */
struct device* device_child_next(const struct device* dev, size_t* iter);

/** @brief Lock #sdm for read.
 *
 * Fails if #sdm is locked for write. There could be many readers, each incrementing #sdm::rlock counter.
 * @retval 1 success.
 * @retval 0 error.
 */
int lock_read(void);

/** @brief Lock #sdm for write.
 *
 * Fails if #sdm is locked for write or read.
 * @retval 1 success.
 * @retval 0 error.
 */
int lock_write(void);

/** @brief Unlock #sdm for read.
 *
 * Each call decreases #sdm::rlock counter. For lock_write() to succeed, number of lock_read() and unlock_read() calls should be equal.
 * @retval 1 success.
 * @retval 0 error.
 */
int unlock_read(void);

/** @brief Unlock #sdm for write.
 *
 * @retval 1 success.
 * @retval 0 error.
 */
int unlock_write(void);

/** @brief Accociate pin with #device.
 *
 * Pin number range is [0,#PINNUM)
 * @param[in]	dev #device handle.
 * @param[in]	pin pin number.
 *
 * @retval 1 success.
 * @retval 0 error.
 */
int pin_request(struct device* dev, short int pin);

/** @brief Free pin from #device.
 *
 * Pin number range is [0,#PINNUM)
 * @param[in]	pin pin number.
 *
 * @retval 1 success.
 * @retval 0 error.
 */
int pin_free(short int pin);

/** @brief Get #device, holding pin.
 *
 * Pin number range is [0,#PINNUM)
 * @param[in]	pin pin number.
 *
 * @return #device handle.
 * @retval NULL error.
 */
struct device* pin_device(short int pin);

/** @brief Copy string to #sdm::strings.
 *
 * Copy \p str to \p buffer from \p start to \p end.
 * @param[in]	str Original string.
 * @param[in]	size Size of string to copy.
 *
 * @return #str handle.
 * @retval NULL error.
 */
struct str* push_str(const char* str, size_t size);

/** @brief Map function for freeing nodes of #device.
 *
 * @param[in]	storage #llist_node storage pointer.
 * @param[in]	storage_sz Size of \p storage.
 * @param[in]	data User supplied data.
 * @param[in]	data_sz Size of \p data.
 */
void __devices_free_nodes(void* storage, const size_t storage_sz,
			  const void* data, size_t data_sz);

/** @brief Map function for freeing nodes of #driver.
 *
 * @param[in]	storage #llist_node storage pointer.
 * @param[in]	storage_sz Size of \p storage.
 * @param[in]	data User supplied data.
 * @param[in]	data_sz Size of \p data.
 */
void __drivers_free_nodes(void* storage, const size_t storage_sz,
			  const void* data, size_t data_sz);

/** @brief Map function for freeing #attr internals.
 *
 * @param[in]	storage #llist_node storage pointer.
 * @param[in]	storage_sz Size of \p storage.
 * @param[in]	data User supplied data.
 * @param[in]	data_sz Size of \p data.
 */
void __free_attr(void* storage, const size_t storage_sz, const void* data,
		 size_t data_sz);

/** @brief Map function for freeing #method internals.
 *
 * @param[in]	storage #llist_node storage pointer.
 * @param[in]	storage_sz Size of \p storage.
 * @param[in]	data User supplied data.
 * @param[in]	data_sz Size of \p data.
 */
void __free_method(void* storage, const size_t storage_sz, const void* data,
		   size_t data_sz);

/** @brief Search function for finding #driver by name.
 *
 * @param[in]	storage #llist_node storage pointer.
 * @param[in]	storage_sz Size of \p storage.
 * @param[in]	data User supplied data.
 * @param[in]	data_sz Size of \p data.
 *
 * @retval 1 match.
 * @retval 0 miss.
 */
int __drivers_search_name(void* storage, const size_t storage_sz,
			  const void* data, size_t data_sz);

/** @brief Search function for finding #method by name.
 *
 * @param[in]	storage #llist_node storage pointer.
 * @param[in]	storage_sz Size of \p storage.
 * @param[in]	data User supplied data.
 * @param[in]	data_sz Size of \p data.
 *
 * @retval 1 match.
 * @retval 0 miss.
 */
int __methods_search_name(void* storage, const size_t storage_sz,
			  const void* data, size_t data_sz);

/** @brief Search function for finding #attr by name.
 *
 * @param[in]	storage #llist_node storage pointer.
 * @param[in]	storage_sz Size of \p storage.
 * @param[in]	data User supplied data.
 * @param[in]	data_sz Size of \p data.
 *
 * @retval 1 match.
 * @retval 0 miss.
 */
int __attrs_search_name(void* storage, const size_t storage_sz,
			const void* data, size_t data_sz);

/** @brief Search function for finding #device reference by name.
 *
 * @param[in]	storage #llist_node storage pointer.
 * @param[in]	storage_sz Size of \p storage.
 * @param[in]	data User supplied data.
 * @param[in]	data_sz Size of \p data.
 *
 * @retval 1 match.
 * @retval 0 miss.
 */
int __refs_search_name(void* storage, const size_t storage_sz,
		       const void* data, size_t data_sz);

/** @brief Search function for finding #device by name.
 *
 * @param[in]	storage #llist_node storage pointer.
 * @param[in]	storage_sz Size of \p storage.
 * @param[in]	data User supplied data.
 * @param[in]	data_sz Size of \p data.
 *
 * @retval 1 match.
 * @retval 0 miss.
 */
int __devices_search_name(void* storage, const size_t storage_sz,
			  const void* data, size_t data_sz);

/**@}*/
